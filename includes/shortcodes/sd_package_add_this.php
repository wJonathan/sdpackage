<?php 
/* --- 
Add This Shortcode

Implements a shortcode to use the Add This Social Media Package
--- */


/* --- Register new Shortcode - [add_this] --- */

add_shortcode('add_this', 'sd_package_add_this');

/* --- The callback function that will replace - [add_this] --- */

function sd_package_add_this(){
	return get_option('add_this_content');
}

?>