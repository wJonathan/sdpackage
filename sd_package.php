<?php
/* ---

Plugin name: SD Package
Plugin URI: http://www.SaavedraDesigns.com/
Description: Will Contain All Plugins Created By SD
Author: SaavedraDesigns
Version: 0.0.1
Author URI: http://www.SaavedraDesigns.com/
--- */

/* --- Load Widgets --- */
include_once('includes/widgets/sd_package_book_us.php');
include_once('includes/widgets/sd_package_calendar_list.php');
include_once('includes/widgets/sd_package_random_ad.php');
include_once('includes/widgets/sd_package_recent_suggestions.php');
include_once('includes/widgets/sd_package_social_media_icons.php');
include_once('includes/widgets/sd_package_stationary_ad.php');
include_once('includes/widgets/sd_package_twitter_feed.php');
include_once('includes/widgets/sd_package_what_were_planning.php');

/* --- Load Shortcodes --- */
include_once('includes/shortcodes/sd_package_add_this.php');
include_once('includes/shortcodes/sd_package_calendar_list.php');
include_once('includes/shortcodes/sd_package_header_image.php');
include_once('includes/shortcodes/sd_package_photos.php');
include_once('includes/shortcodes/sd_package_random_ad.php');
include_once('includes/shortcodes/sd_package_stationary_ad.php');
include_once('includes/shortcodes/sd_package_videos_list.php');
include_once('includes/shortcodes/sd_package_whats_new.php');
include_once('includes/shortcodes/sd_package_whats_next.php');

/* --- Load Admin Tools --- */
include_once('includes/admin/sd_package_metabox.php');
include_once('includes/admin/sd_package_tinymce_buttons.php');

/* --- Load Functions --- */
include_once('includes/sd_package_functions.php');

/* --- Add action Hooks and Functions that should run on load --- */
add_action('admin_menu', 'sd_package_functions::add_menu_item');
add_action('widgets_init', 'sd_package_book_us::register_this_widget');
add_action('widgets_init', 'sd_package_calendar_list::register_this_widget');
add_action('widgets_init', 'sd_package_random_ad::register_this_widget');
add_action('widgets_init', 'sd_package_recent_suggestions::register_this_widget');
add_action('widgets_init', 'sd_package_social_media_icons::register_this_widget');
add_action('widgets_init', 'sd_package_stationary_ad::register_this_widget');
add_action('widgets_init', 'sd_package_twitter_feed::register_this_widget');
add_action('widgets_init', 'sd_package_what_were_planning::register_this_widget');
/* --- EOF --- */
?>