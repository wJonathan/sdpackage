<?php 
/* --- GeneralFuncitons.php --- */

/* --- 
Helper functions that assist the Package
======================================== 
--- */
class sd_package_functions{

	/* --- 
	Adds Menus inside Admin Panel
	============================= 
	--- */
	static function add_menu_item(){
		
		/* --- Adds the Main Menu --- SDPackeage ---*/
		add_menu_page(
			'SD Package Settings | Home', //page title
			'SD Package', //menu title
			'manage_options', //permissions
			'SD_Package', //page-name (used in the URL)
			'sd_package_functions::generate_admin_page' //clicking callback function
		);
		
		/* --- Adds the Sub Menu --- Ad Management ---*/
		add_submenu_page(
			'SD_Package',
			'SD Package Settings | Ad Management',
			'Ad Management',
			'manage_options',
			'SD_Package_Ad_Management',
			'sd_package_functions::generate_ad_management_page'
		);
		
		/* --- Adds the Sub Menu --- Media ---*/
		add_submenu_page(
			'SD_Package',
			'SD Package Settings | Media',
			'Media',
			'manage_options',
			'SD_Package_Media',
			'sd_package_functions::generate_media_page'
		);

				/* --- Adds the Sub Menu --- Social Networking ---*/
		add_submenu_page(
			'SD_Package',
			'SD Package Settings | Social Networking',
			'Social Networking',
			'manage_options',
			'SD_Package_Social_Networking',
			'sd_package_functions::generate_social_networking_page'
		);
		
		/* --- Adds the Sub Menu --- Correspondence ---*/
		add_submenu_page(
			'SD_Package',
			'SD Package Settings | Correspondence',
			'Correspondence',
			'manage_options',
			'SD_Package_Correspondence',
			'sd_package_functions::generate_correspondence_page'
		);
		
		/* --- Adds the Sub Menu --- Company Related ---*/
		add_submenu_page(
			'SD_Package',
			'SD Package Settings | Company Related',
			'Company Related',
			'manage_options',
			'SD_Package_Company_Related',
			'sd_package_functions::generate_company_related_page'
		);
				
				/* --- Adds the Sub Menu --- Company Related ---*/
		add_submenu_page(
			'SD_Package',
			'SD Package Settings | Support Documents',
			'Support Documents',
			'manage_options',
			'SD_Package_Support_Documents',
			'sd_package_functions::generate_support_documents_page'
		);			

	}
	
		
	/* ---  
	Generate the Respective Admin Pages and Handle Forms 
	--- */
	
	/* --- 
	Generate Main Admin Page
	========================
	--- */
	static function generate_admin_page(){
		/* --- Include required scripts/styles --- */
		$sd_plugindir = WP_PLUGIN_URL.'/'.str_replace(basename(__FILE__),"",plugin_basename(__FILE__));
		wp_enqueue_style('sd_package_css', $sd_plugindir.'admin/css/sd_package_styles.css');
		/* --- Tell it where the page Design is --- */
		include('admin/sd_package_index.php');
	}
	
	/* --- 
	Generate Ad Management Page 
	===========================
	
	Contains:
	
	- Random Advertisement
	- Stationary Advertisement
	--- */
	static function generate_ad_management_page(){
		/* --- Include required scripts/styles --- */
		$sd_plugindir = WP_PLUGIN_URL.'/'.str_replace(basename(__FILE__),"",plugin_basename(__FILE__));
		wp_enqueue_style('sd_package_css', $sd_plugindir.'admin/css/sd_package_styles.css');
		wp_enqueue_script('sd_tab_script', $sd_plugindir.'admin/javascript/tabs/sd_package_tab_script.js'); //Custom Script for Tabbing
		wp_enqueue_media(); //To use the Media Library v3.5
		wp_enqueue_script('sd_media_script', $sd_plugindir.'admin/javascript/media/sd-image.js'); //Custom use for the Media Library v3.5
		wp_enqueue_script('jQuery');
		$msg = ''; //Used to display success messages on updates
		
		/* --- 
		Random Advertisement 
		====================
		--- */
		if(!empty($_POST) && check_admin_referer('content_rotation_admin_options_update','content_rotation_admin_nonce')){
			/* --- Setup the array to hold the http:// Promotions --- */
			$promotions_clean = Array();
			/* --- Get the Promotions on Post --- */
			$promotions = $_POST['image_url'];
			$titles = $_POST['title'];
			$alts = $_POST['alt_text'];
			$links = $_POST['content_promo_link'];
			$captions = $_POST['caption'];
			/* --- Loop through promotions to clean the urls and push them to -clean- --- */
			$i = 0;
			foreach($promotions as $promotion){
				$url = esc_url($promotion);
				$title = stripcslashes($titles[$i]);
				$alt = stripcslashes($alts[$i]);
				$link = esc_url($links[$i]);
				$caption = stripcslashes($captions[$i]);
				$promotion = array($url, $title, $alt, $link, $caption);
				if($url != ''){
					array_push($promotions_clean, $promotion);
				}
				$i++;
			}
			/* --- Update to the database the cleaned promotions --- */
			update_option('content_rotation_content_block', $promotions_clean);
			/* --- Display the update message --- */
			$msg = '<div class="updated"><p>Your settings have been <strong>Updated</strong></p></div>';
		}
		/* --- Tell it where the page Design is --- */
		include('admin/sd_package_ad_management.php');
	}
	
	/* --- 
	Generate the Correspondence Page 
	================================
	
	Contains: 
	
	- Book Us
	--- */
	static function generate_correspondence_page(){
		/* --- Include required scripts/styles --- */
		$sd_plugindir = WP_PLUGIN_URL.'/'.str_replace(basename(__FILE__),"",plugin_basename(__FILE__));
		wp_enqueue_style('sd_package_css', $sd_plugindir.'admin/css/sd_package_styles.css');
		
		/* --- 
		Book Us
		=======
		--- */
		$msg = ''; //Used to display success messages on updates
		if(!empty($_POST) && check_admin_referer('bookus_admin_options_update','bookus_admin_nonce')){
			/* --- Update to the database the title and content --- */
			update_option('book_us_title', stripcslashes($_POST['book_us_title']));
			update_option('book_us_content', stripslashes($_POST['content_block']));
			/* --- Display the update message --- */
			$msg = '<div class="updated"><p>Your settings have been <strong>Updated</strong></p></div>';
		}
		/* --- Tell it where the page design is ---*/
		include('admin/sd_package_correspondence.php');		
	}	
	
	/* --- 
	Generate the Company Related Page
	=================================
	
	Contains: 
	
	- Calendar List
	- Recent Suggestions
	- What's New
	- What's Next
	- What We're Planning Tabs
	--- */
	static function generate_company_related_page(){
		$msg = ''; //Used to display success messages on updates
		/* --- Include required scripts/styles --- */
		$sd_plugindir = WP_PLUGIN_URL.'/'.str_replace(basename(__FILE__),"",plugin_basename(__FILE__));
		wp_enqueue_style('sd_package_css', $sd_plugindir.'admin/css/sd_package_styles.css');
		wp_enqueue_script('sd_tab_script', $sd_plugindir.'admin/javascript/tabs/sd_package_tab_script.js'); //Custom Script for Tabbing
		
		/* --- 
		Calendar List 
		=============
		--- */
		if(!empty($_POST) && $_POST['Section'] == 'calendar_list'){
			if(check_admin_referer('calendar_list_admin_options_update','calendar_list_admin_nonce')){
				/* --- Update to the database the  content --- */
				update_option('calendar_list_feed_url', stripslashes($_POST['calendar_list']));
				/* --- Display the update message --- */
				$msg = '<div class="updated"><p>Your settings have been <strong>Updated</strong></p></div>';
			}
		}
		
		/* --- 
		Recent Suggestions 
		==================
		--- */
		if(!empty($_POST) && $_POST['Section'] == 'recent_suggestions'){
			if(check_admin_referer('suggestions_admin_options_update','suggestions_admin_nonce')){
				/* --- Update to the database the title and content --- */
				update_option('recent_suggestions_title', stripcslashes($_POST['recent_suggestions_title']));
				update_option('recent_suggestions_content', stripslashes($_POST['recent_block']));
				/* --- Display the update message --- */
				$msg = '<div class="updated"><p>Your settings have been <strong>Updated</strong></p></div>';
			}
		}

		/* --- 
		What We're Planning 
		===================
		--- */
		if(!empty($_POST) && $_POST['Section'] == 'what_were_planning'){
			if(check_admin_referer('planning_admin_options_update','planning_admin_nonce')){
				/* --- Update to the database the title and content --- */
				update_option('what_were_planning_title', stripcslashes($_POST['what_were_planning_title']));
				update_option('what_were_planning_content', stripslashes($_POST['planning_block']));
				/* --- Display the update message --- */
				$msg = '<div class="updated"><p>Your settings have been <strong>Updated</strong></p></div>';
			}
		}
		
		/* --- Tell it where the page design is ---*/
		include('admin/sd_package_company_related.php');
	}
	
	/* --- 
	Generate the Media Page 
	=======================
	
	Contains:
	
	- Header Image
	- Music List
	- Photo Gallery List
	- Videos List
	--- */
	static function generate_media_page(){
		/* --- Include required scripts/styles --- */
		wp_enqueue_media(); //To use the Media Library v3.5
		$sd_plugindir = WP_PLUGIN_URL.'/'.str_replace(basename(__FILE__),"",plugin_basename(__FILE__));
		wp_enqueue_script('sd_tab_script', $sd_plugindir.'admin/javascript/tabs/sd_package_tab_script.js'); //Custom Script for Tabbing
		wp_enqueue_script('sd_media_script', $sd_plugindir.'admin/javascript/media/sd-image.js'); //Custom use for the Media Library v3.5
		wp_enqueue_script('sd_music_mp3', $sd_plugindir.'admin/javascript/media/sd-music-mp3.js'); //Custom use for the Media Library v3.5
		wp_enqueue_script('sd_music_ogg', $sd_plugindir.'admin/javascript/media/sd-music-ogg.js'); //Custom use for the Media Library v3.5
		wp_enqueue_style('sd_package_css', $sd_plugindir.'admin/css/sd_package_styles.css');
		wp_enqueue_script('jQuery');
		$msg = ''; //Used to display success messages on updates
		
		/* --- 
		Header Image 
		============
		--- */
		if(!empty($_POST) && $_POST['Section'] == 'header_image'){
			if(check_admin_referer('header_image_admin_options_update','header_image_admin_nonce')){
				/* --- Update to the database the content --- */
				update_option('header_image_title', stripcslashes($_POST['title'][0]));
				update_option('header_image_link', esc_url($_POST['header_image_link']));
				update_option('header_image_caption', stripcslashes($_POST['caption'][0]));
				update_option('header_image_alt_text', stripcslashes($_POST['alt_text'][0]));
				update_option('header_image_url', esc_url($_POST['image_url'][0]));
				/* --- Display the update message --- */
				$msg = '<div class="updated"><p>Your settings have been <strong>Updated</strong></p></div>';
			}
		}
		
		/* --- 
		Music List 
		==========
		--- */
		if(!empty($_POST) && $_POST['Section'] == 'music_list'){
			if(check_admin_referer('music_list_admin_options_update','music_list_admin_nonce')){
				/* --- Setup an array to hold the Songs --- */
				$songs_clean = array();
				/* --- Get the songs on Post --- */
				$songs = $_POST['mp3'];
				$song_titles = $_POST['title'];
				$oggs = $_POST['ogg'];
				/* --- Loop through songs to clean the urls and push them to -clean- --- */
				$i = 0;
				foreach($songs as $song){
					$mp3 = esc_url($song);
					$title = stripcslashes($song_titles[$i]);
					$ogg = esc_url($oggs[$i]);
					$song_clean = array($mp3, $title, $ogg);
					if($mp3 != '' && $ogg != ''){
						array_push($songs_clean, $song_clean);
					}
					$i++;
				}
				/* --- Update to the database the content --- */
				update_option('music_list', $songs_clean);
				/* --- Display the update message --- */
				$msg = '<div class="updated"><p>Your settings have been <strong>Updated</strong></p></div>';
			}
		}
		/* --- Tell it where the page design is ---*/
		include('admin/sd_package_media.php');
	}

	/* --- 
	Generate the Social Networking page 
	===================================
	
	Contain:
	
	- Add This
	- Social Media Icons
	- Twitter Feed
	--- */
	static function generate_social_networking_page(){
		/* --- Include required scripts/styles --- */
		$sd_plugindir = WP_PLUGIN_URL.'/'.str_replace(basename(__FILE__),"",plugin_basename(__FILE__));
		wp_enqueue_style('sd_package_css', $sd_plugindir.'admin/css/sd_package_styles.css');
		wp_enqueue_script('sd_tab_script', $sd_plugindir.'admin/javascript/tabs/sd_package_tab_script.js'); //Custom Script for Tabbing
		
		$msg = ''; //Used to display success messages on updates
		
		/* --- 
		Social Media Icons 
		==================
		--- */
		if(!empty($_POST) && $_POST['Section'] == 'social_icons'){
			if(check_admin_referer('social_media_admin_options_update','social_media_admin_nonce')){
				/* --- Update to the database the Links for the Social Medias --- */
				update_option('social_media_youtube', esc_url($_POST['youtube_link']));
				update_option('social_media_twitter', esc_url($_POST['twitter_link']));
				update_option('social_media_facebook', esc_url($_POST['facebook_link']));
				update_option('social_media_linkedin', esc_url($_POST['linkedin_link']));
				/* --- Display the update message --- */
				$msg = '<div class="updated"><p>Your settings have been <strong>Updated</strong></p></div>';
			}
		}
		
		/* --- 
		Add This 
		========
		--- */
		if(!empty($_POST) && $_POST['Section'] == 'add_this'){ 
			if(check_admin_referer('add_this_admin_options_update','add_this_admin_nonce')){
				/* --- Update to the database the content --- */
				update_option('add_this_content', stripslashes($_POST['content_block']));
				/* --- Display the Update Message --- */
				$msg = '<div class="updated"><p>Your settings have been <strong>Updated</strong></p></div>';
			}
		}
		
		/* --- 
		Twitter Feed 
		============
		--- */
		if(!empty($_POST) && $_POST['Section'] == 'twitter_feed'){
			if(check_admin_referer('twitter_feed_admin_options_update','twitter_feed_admin_nonce')){
				/* --- Update to the database the Twitter feed name and Link --- */
				update_option('twitter_name', stripcslashes($_POST['twitter_name']));
				update_option('twitter_number', stripcslashes($_POST['twitter_number']));
				/* --- Display the update message --- */
				$msg = '<div class="updated"><p>Your settings have been <strong>Updated</strong></p></div>';
			}
		}
		/* --- Tell it where the page Design is --- */
		include('admin/sd_package_social_networking.php');
	}
	
		/* --- 
	Generate Support Documents Page
	========================
	--- */
	static function generate_support_documents_page(){
		/* --- Include required scripts/styles --- */
		$sd_plugindir = WP_PLUGIN_URL.'/'.str_replace(basename(__FILE__),"",plugin_basename(__FILE__));
		wp_enqueue_style('sd_package_css', $sd_plugindir.'admin/css/sd_package_styles.css');
		wp_enqueue_script('sd_tab_script', $sd_plugindir.'admin/javascript/tabs/sd_package_tab_script.js'); //Custom Script for Tabbing
		/* --- Tell it where the page Design is --- */
		include('admin/sd_package_support_documents.php');
	}
	
		
	/* ---
	Important Functions for Functionality
	=====================================
	--- */
		
	/* ---
	   get_random_content
	   Fetch and return a piece of random content
	--- */
	static function get_random_content(){
		$sanitized_array = Array();
		$content_block = get_option('content_rotation_content_block');
		foreach($content_block as $block){
			if(isset($block[0]) && $block[0] != ''){
				array_push($sanitized_array, $block);
			}
		}
		$chunk_cnt = count($sanitized_array);
		if($chunk_cnt){
			$n = rand(0, ($chunk_cnt -1));
			return $sanitized_array[$n];
		}else{
			return '';
		}
	}

	/* ---
	   get_content
	   Fetch and return content from the admin panel
	--- */
	static function get_content($instance,$title){
		$content_block = get_option($title);
		if(empty($content_block)){
			return '';
		}
		return $content_block;
	}

	/* ---
	   parse
	   A simple parsing funciton for basic templating
	--- */
	static function parse($tpl, $hash){
		foreach($hash as $key => $value){
			$tpl = str_replace('[+'.$key.'+]', $value, $tpl);
		}
		return $tpl;
	}
	
		/* ---
	Important Functions for Styling Admin
	=====================================
	--- */
		
	
	function get_thickbox_css_all_pages(){
		//register the style handle
		wp_register_style($handle = 'sd_thickbox_css_all', $src=plugins_url('admin/css/sd_package_thickbox.css', __FILE__), $deps = array(), $ver = '1.0.0', $media = 'all');
		// call the style
		wp_enqueue_style('sd_thickbox_css_all');
		
	}
}

/* --- EOF --- */

?>