<?php
/* ---
Videos List Shortcode

Implements a shortcode to use the Videos List
--- */

/* --- Register new Shortcode - [videos_list] --- */

add_shortcode('videos_list', 'sd_package_videos_list');

/* --- The callback function that will replace - [videos_list] --- */

function sd_package_videos_list($attr){

	/* --- Set defaults --- */
	$defaults = array(
	'number_posts' => '3',
	);

	/* --- Set defaults if omitted --- */
	extract(shortcode_atts($defaults, $attr));
	/* --- Set the output --- */
	$sd_plugindir = dirname(plugin_dir_url(__file__));
	$output = "<div class='shortcode sd_package_videos_list'><h3 class='widget-title'>Videos</h3>";
	global $post;
	$args = array('numberposts'=> 1, 'category_name' => 'Video', 'post_parent' => 'Media', 'offset' => 0, 'orderby' => 'post_date', 'order' => 'DESC', 'post_type' => 'post', 'post_status' => 'publish' );
	$myposts = get_posts( $args );
	foreach( $myposts as $post ) :	setup_postdata($post);
	/* --- Setup the output to display the posts --- */
	$output .= '<div class="main-featured-video"><iframe name="video-display" width="272" height="153" src="'.get_post_meta( get_the_ID(), 'sd_package_meta_video_url', true).'" frameborder="0" allowfullscreen></iframe></div><ul class="featured-videos">';
	endforeach;

	global $post;
	$args = array('numberposts'=> $number_posts, 'category_name' => 'video', 'post_parent' => 'Media', 'offset' => 0, 'orderby' => 'post_date', 'order' => 'DESC', 'post_type' => 'post', 'post_status' => 'publish' );
	$myposts = get_posts( $args );
	/* --- Setup the output to display the posts --- */
	foreach( $myposts as $post ) :	setup_postdata($post);
		$output .= '<li class="video"><a href="'.get_post_meta( get_the_ID(), 'sd_package_meta_video_url', true).'"  title="'.get_the_title().'" target="video-display">'.get_the_title().'<img src="'.$sd_plugindir.'/images/blue-arrow.png
" alt="Play Video" class="play-video-btn no-border"/></a></li>';

	endforeach;
	$output .= '</ul><p class="videos-more"><a href="'.site_url().'/media/video" title="View More" class="red-more">More...</a></p></div>';


	/* --- Return the output --- */
	return $output;
}
?>