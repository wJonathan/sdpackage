<?php
/* --- sd_package_social_networking - Social Networking Settings Page --- */
print $msg; 
define('PLUGIN_PATH', WP_PLUGIN_URL.'/'.str_replace(basename(__FILE__),"",plugin_basename(__FILE__)));
$page = 'social-networking';
$tabs = true;
$tab_content = <<<EOD
	<li><a href="javascript:tabSwitch(1, 3, 'sd-tab-', 'sd-content-');" title="Add This" class="active" id="sd-tab-1">Add This</a></li>
	<li><a href="javascript:tabSwitch(2, 3, 'sd-tab-', 'sd-content-');" title="Social Media" id="sd-tab-2">Social Media</a></li>
	<li><a href="javascript:tabSwitch(3, 3, 'sd-tab-', 'sd-content-');" title="Twitter Feed" id="sd-tab-3">Twitter Feed</a></li>
EOD;

require_once(dirname(__FILE__).'/includes/sd_package_masthead.php');?>

                    <div class="settings-section" id="sd-content-1">
                        <div class="section-title">
                            <h4 class="title">Add This Settings</h4>
                        </div>
                        <!-- .section-title -->
                        <div class="section-content">
                            <p>The <em>Add This</em> section may be added to any post/page using the [SD] button in the editor. The editor will allow you to populate a shortcode that was created specifically for What You Want Productions.</p>
                            
                            <p><strong>The Shortcode:</strong></p>
                            <p class="info"><code>[add_this]</code></p>
                            
                            <p>Documentation for the AddThis button can be found on the <a href="http://www.addthis.com" title="+AddThis Website" target="_blank">+AddThis Website</a>. Simply copy and paste the AddThis code below:</p>
                        </div>
                        <!-- .section-content -->
                        <div class="section-actions">
                            <form id="add_this_admin_options_form" action="" method="post">
                            	<?php $content = get_option('add_this_content'); ?>
   								<?php wp_editor($content, 'content_block', $settings = array('media_buttons' => false, 'tinymce' => false)); ?>
   								<input type="hidden" name="Section" value="add_this">												<?php wp_nonce_field('add_this_admin_options_update','add_this_admin_nonce'); ?>
									
									<input type="submit" name="submit" class="button-primary" value="Save Settings" />
									
                            </form>
                        </div>
                        <!-- .section-actions -->
                    </div>
                    <!-- .settings-section -->
                    <div class="settings-section" id="sd-content-2">
                        <div class="section-title">
                            <h4 class="title">Social Media Accounts and Links</h4>
                        </div>
                        <!-- .section-title -->
                        <div class="section-content">
                            <p>The <em>Social Media Accounts and Links</em> can be added to a page by using a Widget under Appearance.</p>
                            <p>If you have any additional accounts that you would like to share with your visitors, <a href="mailto:customerservices@saavedradesigns.com" title="Contact Saavedra Designs" target="_blank">please let us know</a> and we'll be happy to add them for you.</p>
                            
                            <p><strong>Link (URL) to your profile/channel/etc. : </strong></p>
                        </div>
                        <!-- .section-content -->
                        <div class="section-actions">
                        	<!-- <input id="add_icon_button" type="button" value="Add Icon" class="button-secondary" /> -->
                            <form id="social_media_admin_options_form" action="" method="post">
                            	<div class="form-setting">
                                <p><span class="thumbnail"><img class="thumbnail" src="http://www.whatyouwantproductions.com/wp-content/themes/whatyouwantproduction/images/social-media-icons/youtube.png" alt="Youtube" /></span><span class="input"><label>YouTube Channel:</label><input type="text" class="wide-input two-col" id="youtube_link" name="youtube_link" value="<?php print get_option('social_media_youtube'); ?>" /></span><span class="float-catch"></span></p>
                                
		<p><span class="thumbnail"><img class="thumbnail" src="http://www.whatyouwantproductions.com/wp-content/themes/whatyouwantproduction/images/social-media-icons/twitter-2.png" alt="Twitter" /></span><span class="input"><label>Twitter Page:</label><input type="text" class="wide-input" id="twitter_link" name="twitter_link" value="<?php print get_option('social_media_twitter'); ?>" /></span><span class="float-catch"></span></p>
        
        <p><span class="thumbnail"><img class="thumbnail" src="http://www.whatyouwantproductions.com/wp-content/themes/whatyouwantproduction/images/social-media-icons/facebook.png" alt="Facebook" /></span><span class="input"><label>Facebook Page:</label><input type="text" class="wide-input" id="facebook_link" name="facebook_link" value="<?php print get_option('social_media_facebook'); ?>" /></span><span class="float-catch"></span></p>
        
        <p><span class="thumbnail"><img class="thumbnail" src="http://www.whatyouwantproductions.com/wp-content/themes/whatyouwantproduction/images/social-media-icons/linkedin.png" alt="LinkedIn" /></span><span class="input"><label>LinkedIn Page:</label><input type="text" class="wide-input" id="linkedin_link" name="linkedin_link" value="<?php print get_option('social_media_linkedin'); ?>" /></span><span class="float-catch"></span></div>
                            	<!-- <div class="icons">
                            	
                            	</div> -->
                            	<input type="hidden" name="Section" value="social_icons">
                            	<?php wp_nonce_field('social_media_admin_options_update','social_media_admin_nonce'); ?>
                            	<input type="submit" name="submit" class="button-primary" value="Save Settings" />
                            </form>
                        </div>
                        <!-- .section-actions -->
                    </div>
                    <!-- .settings-section -->
                    <div class="settings-section" id="sd-content-3">
                        <div class="section-title">
                            <h4 class="title">Twitter Feed Settings</h4>
                        </div>
                        <!-- .section-title -->
                        <div class="section-content">
                            <p>A <em>Twitter Feed</em> section may be added to any page by using a Widget under Appearance.</p>
                        </div>
                         <!-- .section-content -->
                        <div class="section-actions">
                        	<!-- <input id="add_icon_button" type="button" value="Add Icon" class="button-secondary" /> -->
                            <form id="twitter_feed_admin_options_form" action="" method="post">
                            	<div class="setting-form"><p><span class="section-settings"><label  for="twitter_name">Username: <input type="text" class="wide-input" id="twitter_name" name="twitter_name" value="<?php echo get_option('twitter_name'); ?>" /></label>
                            	<label for="twitter_number">Twitter Consumer API Key (Widget ID Number): <input type="text" class="wide-input" id="twitter_number" name="twitter_number" value="<?php echo get_option('twitter_number'); ?>" /></label></p></span></div>

                               	<input type="hidden" name="Section" value="twitter_feed">
                            	<?php wp_nonce_field('twitter_feed_admin_options_update','twitter_feed_admin_nonce'); ?>
                            	<input type="submit" name="submit" class="button-primary" value="Save Settings" />
                            </form>
                        </div>
                        <!-- .section-actions -->
                    </div>
                    <!-- .settings-section -->
                    
<?php require_once(dirname(__FILE__).'/includes/sd_package_footer.php');

if(!empty($_POST) && $_POST['Section'] == 'social_icons'){
	?>
	<script language="javascript">
		document.getElementById('sd-tab-2').className = "active";
		document.getElementById('sd-tab-1').className = "";
		document.getElementById('sd-content-2').style.display = "block";
		document.getElementById('sd-content-1').style.display = "none";
	</script>
	<?php
}
if(!empty($_POST) && $_POST['Section'] == 'twitter_feed'){
	?>
	<script language="javascript">
		document.getElementById('sd-tab-3').className = "active";
		document.getElementById('sd-tab-1').className = "";
		document.getElementById('sd-content-3').style.display = "block";
		document.getElementById('sd-content-1').style.display = "none";
	</script>
	<?php
}
?>