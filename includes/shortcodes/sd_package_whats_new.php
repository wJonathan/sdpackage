<?php
/* ---
What's New Shortcode

Implements a shortcode to use the What's New
--- */


/* --- Register new Shortcode - [whats_new] --- */

add_shortcode('whats_new', 'sd_package_whats_new');

/* --- The callback function that will replace - [whats_new] --- */

function sd_package_whats_new($attr){

	/* --- Set post count default --- */
	$defaults = array(

	'number_posts' => 5,
	'link' => site_url().'/category/events/',

	);

	/* --- Set defaults if omitted --- */
	extract(shortcode_atts($defaults, $attr));

	/* --- Get the posts ready for display --- */
	$i = 1;
	$output = "";
	global $post;
	$args = array('numberposts'=> $number_posts, 'offset' => 0, 'orderby' => 'post_date', 'order' => 'DESC', 'post_type' => array('event', 'webcast', 'post'), 'post_status' => 'publish' );
	$myposts = get_posts( $args );

	/* --- Setup the output to display the posts --- */
	$output =  "<div class='shortcode sd_package_whats_new'><h3 class='widget-title'>What's New</h3><ul>";
	foreach( $myposts as $post ) :	setup_postdata($post);
    //Thumbnail Image URL
    if(has_post_thumbnail()){
      $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail_size' );
      $thumbURL = $thumb['0'];
    } else{
      $thumbURL = get_site_url().'/wp-content/themes/whatyouwantproduction/images/no-photo-available-280x200.jpg';
    }
		$output .= '<li class="whats-new-post"><span class="post-thumb"><a href="'.get_permalink().'"  title="'.get_the_title().'" class="post-thumb"><img src=
    "'.$thumbURL.'" alt="'.get_the_title().'"/></a></span><span class="post-content"><h4><a href="'.get_permalink().'"  title="'.get_the_title().'" class="post-content">'.get_the_title().'</a></h4><p>'.get_the_excerpt().'</p></span>
					<div class="float-catch"></div>
				</li>';

	$i++;
	endforeach;
	$output .= '</ul><a href="'.$link.'" title="Read More" class="red-more">More...</a></div>';

	/* --- Return the output --- */
	return $output;
}

?>