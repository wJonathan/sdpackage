#WYWP-SDPackage 
v0.0.1

---
##Overview
**WYWP-SDPackage**, is a Wordpress plugin package created for *What You Want Productions*.

###Quick Start
---
####Download
To get the files use one of the following methods

* [Download directly from here](https://bitbucket.org/saavedradesigns/wywp-sdpackage).
*  Clone the repo: `git clone https://wJonathan@bitbucket.org/saavedradesigns/wywp-sdpackage.git`

####Installation
1. To install place the files in the main folder and place in `wp-content/plugins/`
2. Go to the Wordpress plugin section and activate the plugin called *SDPackage*

###Help and Support
---
If you need help with this Wordpress plugin
contact <support@saavedradesigns.com>

