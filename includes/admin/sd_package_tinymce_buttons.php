<?php
/* --- Tiny MCE Buttons --- */


add_action('admin_init', 'add_button');

function add_button(){
	if(current_user_can('edit_posts') && current_user_can('edit_pages')){
		add_filter('mce_external_plugins', 'add_plugin');
		add_filter('mce_buttons', 'register_button');
	}
}

function register_button($buttons){
	array_push($buttons, ' | ', 'buttonPlugin');
	return $buttons;
}

function add_plugin($plugin_array){
	$sd_plugindir = WP_PLUGIN_URL.'/'.str_replace(basename(__FILE__),"",plugin_basename(__FILE__));
	$plugin_array['buttonPlugin'] = $sd_plugindir.'/javascript/buttons/sd_package_tinymce_buttons.js';
	return $plugin_array;
}


?>