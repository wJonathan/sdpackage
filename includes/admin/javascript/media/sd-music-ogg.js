/**
 * Thanks to mojowill For the Tutorial on Thomas Griffin's Github Script
 *
 * https://github.com/thomasgriffin/New-Media-Image-Uploader
 * http://www.mojowill.com/developer/using-the-new-wordpress-3-5-media-manager-in-your-plugin-or-theme/
 *
*/

jQuery(document).ready(function($) {
	//Prepare the variable to hold our custom madia manager.
	var sd_media_frame;
	var formlabel = 0;
	
	//Bind our click event in order to open up a new media experience
	$(document.body).on('click.sdOpenMediaManager', '.sd-open-ogg', function(e) { 
		//sd-open-media will be our form button
		
		//Prevent the default action from occuring.
		e.preventDefault();
		
		//Get our parent element
		formlabel = jQuery(this).parent().parent();
		//alert(formlabel.value);
		
		
		//If frame already exists, re-open it.
		if(sd_media_frame){
			sd_media_frame.open();
			return;
		}
		sd_media_frame = wp.media.frames.sd_media_frame = wp.media({
		
		//Create our media frame
		className: 'media-frame sd_media_frame',
		multiple: false, //Disallow multiple selections
		library: {
			//type: 'audio', //Allow only audio
			type: 'audio/ogg'
			},
		});
		sd_media_frame.on('select', function() {
			//Grab our attachment selection and construct a JSON representation of the model.
			var media_attachment = sd_media_frame.state().get('selection').first().toJSON();
			if(media_attachment.mime == 'audio/ogg'){ 
				//Send the attachment URL to our custom input field via jQuery.
				formlabel.find('input[name="ogg[]"]').val(media_attachment.url);
				formlabel.find('input[name="title[]"]').val(media_attachment.title);
			}else{
				alert("You are trying to insert a file without the correct type to the OGG field! Please check your filetypes and try again");
			}
			
			
		});
		sd_media_frame.open();
	 });
});