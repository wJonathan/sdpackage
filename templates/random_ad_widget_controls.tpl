<!--
This .tpl file is used when editing a widget's options in the WP manager.
It should only contain *elements*; Wordpress will supply the opening and closing <form> tags.

For each key in the ContentRotatorWidget::$control_options array, you will have placeholders for:

[+your_key.id+] - used inside id attributes
[+your_key.name+] - used inside name attributes
[+your_key.value+] - contains the current value of the option

Wordpress appends text tot he names and id's to allow for mulitple instances of the widget so don't hardcode those values here.
-->

<label for="[+seconds_shelf_life.id+]">How long the ad displays (seconds):</label><br />
	<input id="[+seconds_shelf_life.id+]" name="[+seconds_shelf_life.name+]" value="[+seconds_shelf_life.value+]" />

<!-- EOF -->