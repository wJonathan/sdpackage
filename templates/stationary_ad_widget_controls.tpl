<!--
This .tpl file is used when editing a widget's options in the WP manager.
It should only contain *elements*; Wordpress will supply the opening and closing <form> tags.

For each key in the ContentRotatorWidget::$control_options array, you will have placeholders for:

[+your_key.id+] - used inside id attributes
[+your_key.name+] - used inside name attributes
[+your_key.value+] - contains the current value of the option

Wordpress appends text tot he names and id's to allow for mulitple instances of the widget so don't hardcode those values here.
-->
<div id="stationary-ad-wi">
<label for="[+image_url.id+]">Image URL: </label><br />
	<input type="text" class="image_url"  id="[+image_url.id+]" name="[+image_url.name+]" value="[+image_url.value+]" /><br />
<label for="[+link.id+]">Link To: </label><br />
	<input type="text"  id="[+link.id+]" name="[+link.name+]" value="[+link.value+]" /><br />
<label for="[+image_title.id+]">Title: </label><br />
	<input type="text" class="image_title" id="[+image_title.id+]" name="[+image_title.name+]" value="[+image_title.value+]" /><br />
<label for="[+alt_text.id+]">Alt: </label><br />
	<input type="text" class="alt_text" id="[+alt_text.id+]" name="[+alt_text.name+]" value="[+alt_text.value+]" /><br />
<label for="[+image_caption.id+]">Caption: </label><br />
	<input type="text" class="image_caption" id="[+image_caption.id+]" name="[+image_caption.name+]" value="[+image_caption.value+]" /><br />

</div>
<!-- EOF -->