<!--
This .tpl file is used when editing a widget's options in the WP manager.
It should only contain *elements*; Wordpress will supply the opening and closing <form> tags.

For each key in the ContentRotatorWidget::$control_options array, you will have placeholders for:

[+your_key.id+] - used inside id attributes
[+your_key.name+] - used inside name attributes
[+your_key.value+] - contains the current value of the option

Wordpress appends text tot he names and id's to allow for mulitple instances of the widget so don't hardcode those values here.
-->
<label for="[+calendar.id+]">Calendar URL</label><br />*if left empty will default to SD Package*
	<input id="[+calendar.id+]" name="[+calendar.name+]" value="[+calendar.value+]" /><br />
<label for="[+max_number_events.id+]">Max Number of Events</label><br />
	<input id="[+max_number_events.id+]" name="[+max_number_events.name+]" value="[+max_number_events.value+]" /><br />
<!-- EOF -->