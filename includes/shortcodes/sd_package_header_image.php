<?php 
/* --- 
Header Image Shortcode

Implements a shortcode to display the Header Image - Homepage
--- */


/* --- Register new Shortcode - [header_image] --- */

add_shortcode('header_image', 'sd_package_header_image');

/* --- The callback function that will replace - [header_image] --- */

function sd_package_header_image($attr){
	
	/* --- Set defaults --- */
	$defaults = array(
	
	'class' => '',
	'id' => 'header_image',
	'link_url' => get_option("header_image_link"),
	'image_url' => get_option("header_image_url"),
	'image_title' => get_option("header_image_title"),
	'alt_text' => get_option("header_image_alt_text"),
	
	);
	
	/* --- Set defaults if omitted --- */
	extract(shortcode_atts($defaults, $attr));
	
	/* --- Get the output ready for display --- */
	$output = '<section id="header-image-container '.$id.'" class="shortcode sd_package_header_image '.$class.'">';
	
	if(isset($link_url) && $link_url != ''){
		$output .= '<a href="'.$link_url.'" title="'.$image_title.'"><img src="'.$image_url.'" class="header-image" alt="'.$alt_text.'" /></a>';
	}else{
		$output .= '<img src="'.$image_url.'" class="header-image" alt="'.$alt_text.'" />';
	}
	$output .='	</section>';

	/* --- Return the output --- */
	return $output;
}

?>