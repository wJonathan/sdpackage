<?php 
/* --- 
Random Ad Shortcode

Implements a shortcode to use the Random Ad
--- */


/* --- Register new Shortcode - [random_ad] --- */

add_shortcode('random_ad', 'sd_package_random_ad');

/* --- The callback function that will replace - [random_ad] --- */

function sd_package_random_ad($attr){
	
	/* --- Set defaults --- */
	$defaults = array(
	'id' => '',
	'seconds_shelf_life' => 86400,
	
	);
	
	/* --- Set defaults if omitted --- */
	extract(shortcode_atts($defaults, $attr));
	
	if(!isset($id) || $id == ''){
		$output = '';
	}else{

	$val = get_option('manufacture_date_'.$id);
	
	if(!isset($val) || time() >= $val + $seconds_shelf_life ){
		update_option('manufacture_date_'.$id, time());
		$content_all = sd_package_functions::get_random_content();
		$content = $content_all[0];
		$promo_title = $content_all[1];
		$promo_alt = $content_all[2];
		$promo_link = $content_all[3];
		$promo_caption = $content_all[4];
		update_option('content_all'.$id, $content_all);
	}else{
		$content_all = get_option('content_all'.$id);
		//echo $content_all;
		$content = $content_all[0];
		$promo_title = $content_all[1];
		$promo_alt = $content_all[2];
		$promo_link = $content_all[3];
		$promo_caption = $content_all[4];

	}
	
	/* --- Set the output --- */

	$output = "<div class='shortcode sd_package_random_ad'><a href='$promo_link' title='$promo_title' target='_blank' /><figure><img src='$content' alt='$promo_alt' /><figcaption><span class=\"caption-title\">$promo_title</span><span class=\"caption\">$promo_caption</span></figcaption></figure></a></div>";	
	
	}
	
	/* --- Return the output --- */
	return $output;
}

?>