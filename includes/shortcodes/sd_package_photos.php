<?php
/* ---
Photos List Shortcode

Implements a shortcode to display a feed of Media - Photo posts
--- */


/* --- Register new Shortcode - [photos_list] --- */

add_shortcode('photos_list', 'sd_package_photos');

/* --- The callback function that will replace - [photos_list] --- */

function sd_package_photos($attr){

	/* --- Set post count default --- */
	$defaults = array(

	'number_posts' => 3,
	'link' => site_url().'/media/photos/',

	);

	/* --- Set defaults if omitted --- */
	extract(shortcode_atts($defaults, $attr));

	/* --- Get the posts ready for display --- */
	$i = 1;
	$output = "";
	global $post;
	$args = array('numberposts'=> $number_posts, 'offset' => 0, 'category_name' => 'Photos', 'post_parent' => 'Media', 'orderby' => 'post_date', 'order' => 'DESC', 'post_type' => 'post', 'post_status' => 'publish' );
	$myposts = get_posts( $args );

	/* --- Setup the output to display the posts --- */
	$output =  '<div class="shortcode sd_package_photos"><h3 class="widget-title">Photos</h3>';
	foreach( $myposts as $post ) :	setup_postdata($post);
    //Thumbnail Image URL
    if(has_post_thumbnail()){
      $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail_size' );
      $thumbURL = $thumb['0'];
    } else{
      $thumbURL = get_site_url().'/wp-content/themes/whatyouwantproduction/images/no-photo-available-280x200.jpg';
    }

		$output .= '<div class="photo-post photo-post-thumb-'.$i.'">
					<div class="post-thumb">
						<a href="'.get_permalink().'"  title="'.get_the_title().'"><img src="'.$thumbURL.'" alt="'.get_the_title().'"/></a>
					</div>
				</div>';

	$i++;
	endforeach;
	$output .= '<div class="float-catch"></div>
				<div class="photos-more"><p><a href="'.$link.'" title="Read More"  class="red-more">More...</a></p></div></div>';

	/* --- Return the output --- */
	return $output;
}

?>