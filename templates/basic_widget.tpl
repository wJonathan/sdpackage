[+before_widget+]
	[+before_title+]
		[+title+]
	[+after_title+]
	<div class="widget-content">
    [+content+]
  </div>
[+after_widget+]