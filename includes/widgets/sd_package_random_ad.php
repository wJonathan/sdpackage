<?php
/* ---
sd_package_random_ad extends WP_Widget

This implements a Wordpress widget designed to randomize an list of advertisements and display one to the user
--- */

class sd_package_random_ad extends WP_Widget
	{
		public $name = 'SD - Random Advertisement';
		public $description = 'Rotates chunks of contnet on a periodic basis';
		/* ---
		   List all controllable options here along with a default value.
		   The Values can be distinct for each instance of the widget. 
		--- */
		public $control_options = array(
			'seconds_shelf_life' => 86400, //86400 seconds in a day
		);
		
		//!!! Magic Functions
		// The constructor
		
		function __construct(){
			$widget_options = array(
				'classname' => __CLASS__,
				'description' => $this->description,
			);
			parent::__construct(__CLASS__, $this->name,$widget_options,$this->control_options);
						
		}
		
		/* ---
		   Display the widget form in the manager, used for editing its settings
		--- */
		public function form($instance){
			$placeholders = array();
			
			foreach($this->control_options as $key => $val){
				$placeholders[ $key.'.id'] = $this->get_field_id($key);
				$placeholders[ $key.'.name'] = $this->get_field_name($key);
				
				if(isset($instance[$key])){
					$placeholders[ $key.'.value'] = esc_attr($instance[$key]);
				}else{
					$placeholders[ $key.'.value'] = $this->control_options[$key];
				}
			}
			$tpl = file_get_contents(dirname(dirname(dirname(__FILE__))).'/templates/random_ad_widget_controls.tpl');
			print sd_package_functions::parse($tpl, $placeholders);
		} 
		
		//!!! Static Functions
		static function register_this_widget(){
			register_widget(__CLASS__);
		}
		
		/* ---
		   Display content to the front-end.
		--- */
		function widget($args, $instance){
			if(!isset($instance['manufacture_date']) || time() >=$instance['manufacture_date'] + $instance['seconds_shelf_life']){
				$instance['content_all'] = sd_package_functions::get_random_content($instance);
				$instance['content'] = $instance['content_all'][0];
				$instance['promo_title'] = $instance['content_all'][1];
				$instance['promo_alt'] = $instance['content_all'][2];
				$instance['promo_link'] = $instance['content_all'][3];
				$instance['promo_caption'] = $instance['content_all'][4];
				$instance['content_all'] = '';
				$instance['manufacture_date'] = time();
				$all_instances = $this->get_settings();
				$all_instances[$this->number] =$instance;
				$this->save_settings($all_instances);
			}
			$placeholders = array_merge($args, $instance);
			$tpl = file_get_contents(dirname(dirname(dirname(__FILE__))).'/templates/random_ad_widget.tpl');
			print sd_package_functions::parse($tpl, $placeholders);
		}
		
	}
	
	/* --- EOF --- */

?>