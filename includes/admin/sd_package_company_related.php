<?php
/* --- sd_package_company_related - Company Related Settings Page --- */
print $msg; 
define('PLUGIN_PATH', WP_PLUGIN_URL.'/'.str_replace(basename(__FILE__),"",plugin_basename(__FILE__)));
$page = 'company-related';
$tabs = true;

$tab_content = <<<EOD
	<li><a href="javascript:tabSwitch(1, 5, 'sd-tab-', 'sd-content-');" title="Calendar List" class="active" id="sd-tab-1">Calendar List</a></li>
	<li><a href="javascript:tabSwitch(2, 5, 'sd-tab-', 'sd-content-');" title="Recent Suggestions" id="sd-tab-2">Recent Suggestions</a></li>
	<li><a href="javascript:tabSwitch(3, 5, 'sd-tab-', 'sd-content-');" title="What&apos;s New" id="sd-tab-3">What&apos;s New</a></li>
	<li><a href="javascript:tabSwitch(4, 5, 'sd-tab-', 'sd-content-');" title="What&apos;s Next" id="sd-tab-4">What&apos;s Next</a></li>
	<li><a href="javascript:tabSwitch(5, 5, 'sd-tab-', 'sd-content-');" title="What We&apos;re Planning" id="sd-tab-5">What We&apos;re Planning</a></li>
EOD;

require_once(dirname(__FILE__).'/includes/sd_package_masthead.php');?>
                    <div id="sd-content-1">
                        <div class="settings-section" id="">
                            <div class="section-title">
                                <h4 class="title">Calendar List View Settings</h4>
                            </div>
                            <!-- .section-title -->
                            <div class="section-content">
                               <p>Below you may add/edit the Feed URL to the Calendar on the List View page.</p>
                            </div>
                            <!-- .section-content -->
                            <div class="section-actions">
                                <form id="calendar_list_admin_options_form" action="" method="post">
                                    <div class="setting-form"><p><span class="section-settings"><label for="calendar_list">Feed URL: 
                                    <textarea rows="5" cols="50" id="calendar_list" name="calendar_list" class="wide-input"><?php print get_option('calendar_list_feed_url'); ?></textarea></label></span></p></div>
                                    <input type="hidden" name="Section" value="calendar_list">												<?php wp_nonce_field('calendar_list_admin_options_update','calendar_list_admin_nonce'); ?>
                                        
                                        <input type="submit" name="submit" class="button-primary" value="Save Settings" />
                                        
                                </form>
                            </div>
                            <!-- .section-actions -->
                        </div>
                        
                        <!-- .settings-section -->
                        <div class="settings-section" id="">
                            <div class="section-title">
                                <h4 class="title">How Does it Work?</h4>
                            </div>
                            <!-- .section-title -->
                            <div class="section-content">
                               <p>You can also place a calendar list on a page and/or post via the [SD] button in the editor- or through a Widget under Appearance. The editor will allow you to populate a shortcode (displayed below) that was created specifically for What You Want Productions.
                                <p><strong>The Shortcode:</strong></p>
                                <p class="info"><code>[calendar_list number_posts="" calendar="" link="" list_title=""]</code></p>
                                
                                <p><strong>What is <code>number_posts</code>?</strong></p>
                                <p>The <code>number_posts</code> is how many calendar items you would like to display. Default is three (3).</p>
                                
                                <p><strong>What is <code>calendar</code>?</strong></p>
                                <p>The <code>calendar</code> is the feed URL to the calendar you would like to use (i.e. a Google Calendar URL). Defaults to the one above. </p>
                                
                                <p><strong>What is <code>link</code>?</strong></p>
                                <p>The <code>link</code> is the link to the calendar being referred to (e.g. the Calendar page). By default it leads to the Calendar List View page.</p>
                                
                                <p><strong>What is <code>list_title</code>?</strong></p>
                                <p>The <code>list_title</code> is the title for the calendar, which is displayed about the calendar.</p>
    
                            </div>
                            <!-- .section-content -->
                            <div class="section-actions">
                            </div>
                            <!-- .section-actions -->
                        </div>
                        <!-- .settings-section -->
                    </div>
                    <!-- #sd-content-2 -->
                    
                    <div class="settings-section" id="sd-content-2">
                        <div class="section-title">
                            <h4 class="title">Recent Suggestions</h4>
                        </div>
                        <!-- .section-title -->
                        <div class="section-content">
                            <p>The <em>Recent Suggestions</em> section is placed on a page using a Widget under Appearance. The following is the content the widget displays:</p>
                        </div>
                        <!-- .section-content -->
                        <div class="section-actions">
                            <form id="suggestions_admin_options_form" action="" method="post">
                            	<?php $suggestions = get_option('recent_suggestions_content'); ?>
                            	<div class="setting-form"><p><span class="section-settings"><label for="recent_suggestions_title">Title: <input type="text" id="recent_suggestions_title" class="recent_suggestions_title" name="recent_suggestions_title" value="<?php echo get_option('recent_suggestions_title'); ?>" /></label></span></p></div>
		<?php wp_editor($suggestions, 'recent_block', $settings = array('media_buttons' => true)); ?>
                            	<input type="hidden" name="Section" value="recent_suggestions">
                            	<?php wp_nonce_field('suggestions_admin_options_update','suggestions_admin_nonce'); ?>
                            	<input type="submit" name="submit" class="button-primary" value="Save Settings" />
                            </form>
                        </div>
                        <!-- .section-actions -->
                    </div>
                    <!-- .settings-section -->
                    <div class="settings-section" id="sd-content-3">
                        <div class="section-title">
                            <h4 class="title">What's New Information</h4>
                        </div>
                        <!-- .section-title -->
                        <div class="section-content">
                            <p>The <em>What's New</em> section may be placed on a page and/or post using the [SD] button in the editor. The editor will allow you to populate a shortcode that was created specifically for What You Want Productions.</p>
                            <p><strong>The Shortcode:</strong></p>
                            <p class="info"><code>[whats_new number_posts="" link=""]</code></p>
                            
                            <p><strong>What is <code>number_posts</code>?</strong></p>
                            <p>The <code>number_posts</code> is how many posts you would like to display. Default is five (5).</p>
                            
                            <p><strong>What is <code>link</code>?</strong></p>
                            <p>The <code>link</code> is the URL that you would like the visitor to be redirected to when they click on the "More" link. By default it leads to the main Events page.</p>
                            
                        </div>
                         <!-- .section-content -->
                    </div>
                    <!-- .settings-section -->
                    <div class="settings-section" id="sd-content-4">
                        <div class="section-title">
                            <h4 class="title">What's Next Information</h4>
                        </div>
                        <!-- .section-title -->
                        <div class="section-content">
                            <p>The <em>What's Next</em> section may be added to a post/page using the [SD] button in the editor. The editor will allow you to populate a shortcode that was created specifically for What You Want Productions.</p>
                            
                            <p><strong>The Shortcode:</strong></p>
                            <p class="info"><code>[whats_next number_posts="" calendar="" link=""]</code></p>
                            
                            <p><strong>What is <code>number_posts</code>?</strong></p>
                            <p>The <code>number_posts</code> is how many calendar items you would like to display. Default is five (5).</p>
                            
                            <p><strong>What is <code>calendar</code>?</strong></p>
                            <p>The <code>calendar</code> option is the calendar feed URL you would like to use to display the events (e.g. a Google Calendar feed URL). This option is required.</p>
                            
                            <p><strong>What is <code>link</code>?</strong></p>
                            <p>The <code>link</code> is the URL that you would like the visitor to be redirected to when they click on the "More" link. By default it leads to the Calendar List View page.</p>

                        </div>
                         <!-- .section-content -->
                    </div>
                    <!-- .settings-section -->
                    <div class="settings-section" id="sd-content-5">
                        <div class="section-title">
                            <h4 class="title">What We're Planning</h4>
                        </div>
                        <!-- .section-title -->
                        <div class="section-content">
                            <p>The <em>What We're Planning</em> section may be added to a page by using a Widget under Appearance. The following is the content the widget displays.</p>
                        </div>
                         <!-- .section-content -->
                        <div class="section-actions">
                        	<!-- <input id="add_icon_button" type="button" value="Add Icon" class="button-secondary" /> -->
                            <form id="planning_admin_options_form" action="" method="post">
                            	<?php $planning = get_option('what_were_planning_content'); ?>
                            	<div class="setting-form"><p><span class="section-settings"><label for="">Title: <input type="text" class="what_were_planning_title" id="what_were_planning_title" name="what_were_planning_title" value="<?php echo get_option('what_were_planning_title'); ?>"></label></span></p></div>
		<?php wp_editor($planning, 'planning_block', $settings = array('media_buttons' => true)); ?>
                               	<input type="hidden" name="Section" value="what_were_planning">
                            	<?php wp_nonce_field('planning_admin_options_update','planning_admin_nonce'); ?>
                            	<input type="submit" name="submit" class="button-primary" value="Save Settings" />
                            </form>
                        </div>
                        <!-- .section-actions -->
                    </div>
                    
<?php 
require_once(dirname(__FILE__).'/includes/sd_package_footer.php');

if(!empty($_POST) && $_POST['Section'] == 'recent_suggestions'){
	?>
	<script language="javascript">
		document.getElementById('sd-tab-2').className = "active";
		document.getElementById('sd-tab-1').className = "";
		document.getElementById('sd-content-2').style.display = "block";
		document.getElementById('sd-content-1').style.display = "none";
	</script>
	<?php
}
if(!empty($_POST) && $_POST['Section'] == 'what_were_planning'){
	?>
	<script language="javascript">
		document.getElementById('sd-tab-5').className = "active";
		document.getElementById('sd-tab-1').className = "";
		document.getElementById('sd-content-5').style.display = "block";
		document.getElementById('sd-content-1').style.display = "none";
	</script>
	<?php
}
?>