<?php
/* ---
sd_package_twitter_feed extends WP_Widget

This implements a Wordpress widget designed to display a Twitter Feed
--- */

class sd_package_twitter_feed extends WP_Widget
	{
		public $name = 'SD - Twitter Feed';
		public $description = 'Displays the Twitter Feed';
		/* ---
		   List all controllable options here along with a default value.
		   The Values can be distinct for each instance of the widget.
		--- */
		public $control_options = array(
		);

		//!!! Magic Functions
		// The constructor

		function __construct(){
			$widget_options = array(
				'classname' => __CLASS__,
				'description' => $this->description,
			);
			parent::__construct(__CLASS__, $this->name,$widget_options,$this->control_options);
		}

		/* ---
		   Display the widget form in the manager, used for editing its settings
		--- */
		public function form($instance){
			print('Please use the SD Package Dashboard');
		}

		//!!! Static Functions
		static function register_this_widget(){
			register_widget(__CLASS__);
		}

		/* ---
		   Display content to the front-end.
		--- */
		function widget($args, $instance){
			$instance['content_name'] = sd_package_functions::get_content($instance, 'twitter_name');
			$instance['content_url'] = sd_package_functions::get_content($instance, 'twitter_feed_url');
			echo $args['before_widget'];
			include(plugin_dir_path(__FILE__).'includes/sd_package_twitter_feed_init.php');
			?>
		    <div class="twitter-feed-widget-container">
		        <div class="twitter-feed-container left">
		            <div id="tweetContainer">
		                
		            </div>
		            <p class="left twitter-name"><strong>@<?php echo $instance['content_name'];?></strong></p>
		            <p class="right twitter-url"><a href="<?php echo $instance['content_url'];?>" title="Follow us on Twitter" target="_blank">Follow us on Twitter</a></p>
		            <div class="float-catch"></div>
		        </div>
		        <div class="twitter-feed-icon right"></div>
		        <div class="float-catch"></div>
		    </div>

		    <div class="float-catch"></div>
		    <?php
			echo $args['after_widget'];
		}

	}

	/* --- EOF --- */

?>