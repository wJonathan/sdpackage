<?php
/* ---
sd_package_calendar_list extends WP_Widget

This implements a Wordpress widget designed to Display the Calendar List Widget 
--- */

class sd_package_calendar_list extends WP_Widget
	{
		public $name = 'SD - Calendar List Widget';
		public $description = 'Displays the List Calendar';
		
		/* ---
		   List all controllable options here along with a default value.
		   The Values can be distinct for each instance of the widget. 
		--- */
		public $control_options = array(
			'max_number_events' => '3',
			'calendar' => '',

		);
		
		//!!! Magic Functions
		// The constructor
		
		function __construct(){
			$widget_options = array(
				'classname' => __CLASS__,
				'description' => $this->description,
			);
			parent::__construct(__CLASS__, $this->name,$widget_options,$this->control_options);
		}
		
		/* ---
		   Display the widget form in the manager, used for editing its settings
		--- */
		public function form($instance){
			$placeholders = array();
			
			foreach($this->control_options as $key => $val){
				$placeholders[ $key.'.id'] = $this->get_field_id($key);
				$placeholders[ $key.'.name'] = $this->get_field_name($key);
				
				if(isset($instance[$key])){
					$placeholders[ $key.'.value'] = esc_attr($instance[$key]);
				}else{
					$placeholders[ $key.'.value'] = $this->control_options[$key];
				}
			}
			$tpl = file_get_contents(dirname(dirname(dirname(__FILE__))).'/templates/calendar_list_widget_controls.tpl');
			print sd_package_functions::parse($tpl, $placeholders);
		} 
		
		//!!! Static Functions
		static function register_this_widget(){
			register_widget(__CLASS__);
		}
		
		/* ---
		   Display content to the front-end.
		--- */
		function widget($args, $instance){
			$calendar = $instance['calendar'];
			if($calendar == ''){
				$calendar = get_option('calendar_list_feed_url');
			}
			echo $args['before_widget'];
		    echo '<h3 class="widget-title">Calendar</h3><ul class="calendar-list">';
			
		/* --- Set the current Date --- */
	date_default_timezone_set('America/Los_Angeles');
	
	/* --- Set Today's Date --- */
	$today = date('Y-m-'.date('d', mktime(0, 0, 0, date('m'), date('d')-1, date('Y'))).'\TH:i:sP');
	
	/* --- Create new Dom Object --- */
	$dom_object = new DOMDocument();
	
	/* --- Load XML -- */
	$dom_object->load($calendar."?orderby=starttime&sortorder=ascending&max-results=".$instance['max_number_events']."&start-min=".$today);
			 
	/* --- Get the Entry from the XML --- */
	$item = $dom_object->getElementsByTagName("entry");
	$i = 0;
	
	/* --- Get the Content from the XML --- */		 
	foreach( $item as $value ){
		$titles = $value->getElementsByTagName("title");
		$title  = $titles->item(0)->nodeValue;
			 
		$times = $value->getElementsByTagName("when");
		$startTime = $times->item(0)->getAttributeNode("startTime")->value;
		$endTime = $times->item(0)->getAttributeNode("endTime")->value;
		$eventDate = date( "M d", strtotime( $startTime ) );
		$time = date( "F j, Y - h:i A", strtotime( $startTime ) );
		$endingtime = date( "F j, Y - h:i A", strtotime( $endTime ) );
			 
		$location = $value->getElementsByTagName("where");
		$eventLocation = $location->item(0)->getAttributeNode("valueString")->value;
		$eventURL= $value->getElementsByTagName("link");
		$eventLinkURL=$eventURL->item(0)->getAttributeNode("href")->value;
		
			 
			 echo '<li class="event-item"><span class="event-date"><h4 class="event-date">'.$eventDate.'</h4></span><span class="event-description"><h4 class="event-title">'.$title.'</h4><p><strong>When:</strong> '.$time.' to '.$endingtime.'</p><p><strong>Where:</strong> '.$eventLocation.'</p><p><a href="'.$eventLinkURL.'" title="Event Details" target="_blank" >More Details...</a></p></span><span class="float-catch"></span></li>';
			 $i++;
			 
		 }
			
			echo "</ul><p>Showing ".$i." upcoming events.</p>";
		    echo $args['after_widget'];

		}
		
	}
	
	/* --- EOF --- */

?>