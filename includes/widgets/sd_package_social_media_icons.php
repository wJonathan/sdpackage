<?php
/* ---
sd_package_social_media_icons extends WP_Widget

This implements a Wordpress widget designed to display icons for the Social Media Sidebar
--- */

class sd_package_social_media_icons extends WP_Widget
	{
		public $name = 'SD - Social Media Icons';
		public $description = 'Displays the Social Media for the Sidebar';
		/* ---
		   List all controllable options here along with a default value.
		   The Values can be distinct for each instance of the widget. 
		--- */
		public $control_options = array(
		);
		
		//!!! Magic Functions
		// The constructor
		
		function __construct(){
			$widget_options = array(
				'classname' => __CLASS__,
				'description' => $this->description,
			);
			parent::__construct(__CLASS__, $this->name,$widget_options,$this->control_options);
		}
		
		/* ---
		   Display the widget form in the manager, used for editing its settings
		--- */
		public function form($instance){
			print('Please use the SD Package Dashboard');
		} 
		
		//!!! Static Functions
		static function register_this_widget(){
			register_widget(__CLASS__);
		}
		
		/* ---
		   Display content to the front-end.
		--- */
		function widget($args, $instance){
			$instance['youtube'] = sd_package_functions::get_content($instance, 'social_media_youtube');
			$instance['twitter'] = sd_package_functions::get_content($instance, 'social_media_twitter');
			$instance['facebook'] = sd_package_functions::get_content($instance, 'social_media_facebook');
			$instance['linkedin'] = sd_package_functions::get_content($instance, 'social_media_linkedin');
			
			echo $args['before_widget'];
			print('<ul class="social-media-widget">');
			if(isset($instance['youtube']) && $instance['youtube'] != ''){
				print('<li><a href="'.$instance['youtube'].'" target="_blank" title="Watch our videos on Youtube"><img src="http://www.preview.saavedradesigns.com/What-You-Want-Productions/wp-content/themes/wywp/images/social-media-icons/youtube.png" alt="Youtube" /></a></li>');
			}
			if(isset($instance['twitter']) && $instance['twitter'] != ''){
				print('<li><a href="'.$instance['twitter'].'" target="_blank" title="Follow us on Twitter"><img src="http://www.preview.saavedradesigns.com/What-You-Want-Productions/wp-content/themes/wywp/images/social-media-icons/twitter-2.png" alt="Twitter" /></a></li>');
			}
			if(isset($instance['facebook']) && $instance['facebook'] != ''){
				print('<li><a href="'.$instance['facebook'].'" target="_blank" title="Like us on Facebook"><img src="http://www.preview.saavedradesigns.com/What-You-Want-Productions/wp-content/themes/wywp/images/social-media-icons/facebook.png" alt="Facebook" /></a></li>');
			}
			if(isset($instance['linkedin']) && $instance['linkedin'] != ''){
				print('<li><a href="'.$instance['linkedin'].'" target="_blank" title="Add us to your network on LinkedIn"><img src="http://www.preview.saavedradesigns.com/What-You-Want-Productions/wp-content/themes/wywp/images/social-media-icons/linkedin.png" alt="LinkedIn" /></a></li>');
			}
			//if(isset() &&  != ''){ print(''); }
			
	    	print ('</ul> ');
	    	echo $args['after_widget'];

		}
		
	}
	
	/* --- EOF --- */

?>