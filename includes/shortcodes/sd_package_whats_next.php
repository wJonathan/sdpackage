<?php
/* ---
What's Next Shortcode

Implements a shortcode to display What's Next
--- */


/* --- Register new Shortcode - [whats_next] --- */

add_shortcode('whats_next', 'sd_package_whats_next');

/* --- The callback function that will replace - [whats_next] --- */

function sd_package_whats_next($attr){

	/* --- Set post count default --- */
	$defaults = array(

	'number_posts' => 5,
	'calendar' => '',
	'link' => site_url().'/events/calendar/',

	);

	/* --- Set defaults if omitted --- */
	extract(shortcode_atts($defaults, $attr));

	/* --- Get the calendar ready for display --- */
	$output = "<div class='shortcode sd_package_whats_next'><h3 class='widget-title'>What's Next</h3>";
	/* --- Set the current Date --- */
	date_default_timezone_set('America/Los_Angeles');

	/* --- Create new Dom Object --- */
	$dom_object = new DOMDocument();

	/* --- Load XML -- */
	$dom_object->load($calendar."?orderby=starttime&sortorder=ascending&max-results=".$number_posts."&start-min=".date("c"));

	/* --- Get the Entry from the XML --- */
	$item = $dom_object->getElementsByTagName("entry");

	/* --- Get the Titles from the XML --- */
	foreach( $item as $value ){
		$titles = $value->getElementsByTagName("title");
		$title  = $titles->item(0)->nodeValue;

		$dates = $value->getElementsByTagName("summary");

		$date  = $dates->item(0)->nodeValue;
		list($when, $weekday, $month, $dayAndYear, $extra) = split(" ",$date);
		list($day, $year) = split(",",$dayAndYear);

		$output .= '<div class="event-item"><h4><a href="'.$link.'">'.$month.' '.$day.'</a></h4>'.$title.'</div>';
		}
	$output .= '<a href="'.$link.'" title="Read More" class="red-more">More...</a></div>';

	/* --- Return the output --- */
	return $output;
}

?>