<?php 
/* --- 
Stationary Advertisement Shortcode

Implements a shortcode to use the Stationary Advertisement
--- */


/* --- Register new Shortcode - [stationary_ad] --- */

add_shortcode('stationary_ad', 'sd_package_stationary_ad');

/* --- The callback function that will replace - [stationary_ad] --- */

function sd_package_stationary_ad($attr){
	
	/* --- Set defaults --- */
	$defaults = array(
	'id' => '',
	'class' => '',
	'image_title' => '',
	'alt_text' => 'Promotion',
	'image_url' => '',
	'link_to' => '',
	'image_caption' => '',
	);
	
	/* --- Set defaults if omitted --- */
	extract(shortcode_atts($defaults, $attr));
	/* --- Set the output --- */
	$output = '<div';
	if(isset($id) && $id != ''){
		$output .= " id='$id'";
	}
	$output .= ' class="shortcode sd_package_stationary_ad';
	if(isset($class) && $class != ''){
		$output .= " ".$class;
	}
	$output .= '">';
	if(isset($link_to) && $link_to != ''){
		$output .= "<a href='$link_to' target='_blank'";
		if(isset($image_title) && $image_title !=''){
			$output .= "title='$image_title'";
		}
		$output .= "/><figure><img src='$image_url' alt='$alt_text' /><figcaption><span class=\"caption-title\">$image_title</span><span class=\"caption\"> $image_caption</span></figcaption></figure></a></div>";
	}else{
		$output .= "<figure><img src='$image_url' alt='$alt_text' /><figcaption><span class=\"caption-title\">$image_title</span><span class=\"caption\"> $image_caption</span></figcaption></figure></div>";
	}
	
	
	
	/* --- Return the output --- */
	return $output;
}

?>