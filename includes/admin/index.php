<div class="wrap">
	<h2>SD Package Dashboard</h2>
    <p>Welcome to your...</p>
    
	<ul id="sd-nav" class="dropdown">
		<li><a href="">Advertisements</a>
        	<ul class="subnav">
            	<li><a href="">Artist Promotions</a></li>
                <li><a href="">Promotions</a></li>
            </ul>
        </li>
		<li><a href="">Company Related</a>
        	<ul>
            	<li><a href="">Calendar List</a></li>
                <li><a href="">Recent Suggestions</a></li>
                <li><a href="">What We're Planning</a></li>
            </ul>
        </li>
        <li><a href="">Correspondence</a>
        	<ul>
            	<li><a href="">Book Us</a></li>
            	<li><a href="">Company Contact</a></li>
                <li><a href="">Custom Emails</a></li>
            </ul>
        </li>
        <li><a href="" >Media</a>
        	<ul>
        		<li><a href="">Header Image</a></li>
        		<li><a href="">Songs</a></li>
        	</ul>
        </li>
		<li><a href="">Social Networking</a>
        	<ul>
            	<li><a href="">AddThis</a></li>
            	<li><a href="">Social Media Links</a></li>
            	<li><a href="">Twitter Feed</a></li>
            </ul>
        </li>
        <div class="clear-float"></div>
	</ul>
    
    <div id="package-wrapper">
    	<p>Content</p>
    </div>
</div>