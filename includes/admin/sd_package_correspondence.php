<?php
/* --- sd_package_correspondence - Correspondence Settings Page --- */
print $msg; 
define('PLUGIN_PATH', WP_PLUGIN_URL.'/'.str_replace(basename(__FILE__),"",plugin_basename(__FILE__)));
$page = "correspondence";

$tabs = true;
$tab_content = <<<EOD
	<li><a href="javascript:tabSwitch(1, 3, 'sd-tab-', 'sd-content-');" title="Book Us" class="active" id="sd-tab-1">Book Us</a></li>
EOD;

require_once(dirname(__FILE__).'/includes/sd_package_masthead.php');?>

                    <div class="settings-section" id="sd-content-1">
                        <div class="section-title">
                            <h4 class="title">Book Us Settings</h4>
                        </div>
                        <!-- .section-title -->
                        <div class="section-content">
                        	<p>The <em>Book Us</em> section may be added to a page using a Widget under Appearance. The following is the content the widget displays.</p>
                        </div>
                        <!-- .section-content -->
                        <div class="section-actions">
                            <form id="bookus_admin_options_form" action="" method="post">
                            	<div class="setting-form"><p><span class="section-settings"><label for="book_us_title">Title: <input type="text" id="book_us_title" class="book_us_title wide-input" name="book_us_title" value="<?php echo get_option('book_us_title'); ?>" /></label></span></p><?php $content = get_option('book_us_content'); ?> 
		<?php wp_editor($content, 'content_block', $settings = array('media_buttons' => true)); ?>
                            	</div>
												<?php wp_nonce_field('bookus_admin_options_update','bookus_admin_nonce'); ?>
									
									<input type="submit" name="submit" class="button-primary" value="Save Settings" />
									
                            </form>
                        </div>
                        <!-- .section-actions -->
                    </div>
                    <!-- .settings-section -->
                    
<?php require_once(dirname(__FILE__).'/includes/sd_package_footer.php'); ?>