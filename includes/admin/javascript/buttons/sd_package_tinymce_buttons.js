(function(){
	var sd_path;
	tinymce.create('tinymce.plugins.buttonPlugin', {
		init : function(ed, url) {
		n = url.split('//',2);
		sd_path = 'http://'+n[1];
		},
		createControl : function(n, cm) {
			switch(n){
				case 'buttonPlugin':

					var c = cm.createSplitButton('buttonPlugin', {
						title : 'SD Package Shortcodes',
						image: sd_path+"/images/sd_package_icon.png",
						onclick: function() {
							//tinyMCE.activeEditor.windowManager.alert('Button Was clicked.');
						}
					});

					c.onRenderMenu.add(function(c,m) {
						m.add({ title: 'Add This', onclick : function(){
							tinyMCE.activeEditor.selection.setContent('[add_this]');
						}});
						m.add({ title: 'Photos List', onclick : function(){
							var width = jQuery(window).width(), H = jQuery(window).height(), W = (720 < width) ? 720 : width;
							W = W - 80;
							H = H - 84;
							tb_show('Photos List Shortcode', '#TB_inline?width=' + W + '&height=' + H + '&inlineId=photos-list-form');
						}});
						m.add({ title: 'Random Ad', onclick : function(){
							var width = jQuery(window).width(), H = jQuery(window).height(), W = (720 < width) ? 720 : width;
							W = W - 80;
							H = H - 84;
							tb_show('Random Ad Shortcode', '#TB_inline?width=' + W + '&height=' + H + '&inlineId=random-ad-form');
						}});
						m.add({ title: 'Stationary Ad', onclick : function(){
							var width = jQuery(window).width(), H = jQuery(window).height(), W = (720 < width) ? 720 : width;
							W = W - 80;
							H = H - 84;
							tb_show('Stationary Ad Shortcode', '#TB_inline?width=' + W + '&height=' + H + '&inlineId=stationary-ad-form');
						}});
						m.add({ title: 'Videos List', onclick : function(){
							var width = jQuery(window).width(), H = jQuery(window).height(), W = (720 < width) ? 720 : width;
							W = W - 80;
							H = H - 84;
							tb_show('Videos List Shortcode', '#TB_inline?width=' + W + '&height=' + H + '&inlineId=videos-list-form');
						}});
						m.add({ title: 'What\'s New', onclick : function(){
							var width = jQuery(window).width(), H = jQuery(window).height(), W = (720 < width) ? 720 : width;
							W = W - 80;
							H = H - 84;
							tb_show('What\'s New', '#TB_inline?width=' + W + '&height=' + H + '&inlineId=whats-new-form');
						}});
						m.add({ title: 'What\'s Next', onclick : function(){
							var width = jQuery(window).width(), H = jQuery(window).height(), W = (720 < width) ? 720 : width;
							W = W - 80;
							H = H - 84;
							tb_show('What\'s Next', '#TB_inline?width=' + W + '&height=' + H + '&inlineId=whats-next-form');
						}});
						m.add({ title: 'Downloadable Content', onclick : function(){
							var width = jQuery(window).width(), H = jQuery(window).height(), W = (720 < width) ? 720 : width;
							W = W - 80;
							H = H - 84;
							tb_show('Downloadable Content', '#TB_inline?width=' + W + '&height=' + H + '&inlineId=downloable-content-form');
						}});

					});

					//Return split button instance
					return c;

					}
				return null;
			}

		});
	tinymce.PluginManager.add('buttonPlugin', tinymce.plugins.buttonPlugin);
	//tinymce.PluginManager.add('quote', tinymce.plugins.buttonPlugin);


	/* --- Display the Form in an Iframe --- */
	jQuery(function() {

		 /* --- Display the Photos List - [photos_list] --- */
		var form_photos = jQuery('<div id="photos-list-form"><div id="photos-list" class="mce-section"><span class="section-settings"><label for="number_posts">Number of Posts to Display: <input type="text" name="number_posts" id="number_posts" value="" /><small>Defaults to 3</small></label>\
		<label for="link">Link to: <input type="text" name="link" id="link" value="" /><small>Defaults to siteurl + /media/photos/</small></label></span>\
		<p class="submit"><input type="button" id="photos-list-submit" class="button-primary" value="Insert Photos List Settings" name="submit" /></p></div></div>');

		var table = form_photos.find('div');
		form_photos.appendTo('body').hide();

		form_photos.find('#photos-list-submit').click(function() {

		var options = {
			'number_posts' : '',
			'link' : '',
			};

			var shortcode = '[photos_list';

			for(var index in options){
				var value = jQuery('#photos-list #' + index).val();

				if(value !== options[index])
					shortcode += ' ' + index + '="' + value + '"';

			}

			shortcode += ']';

			tinyMCE.activeEditor.execCommand('mceInsertContent', 0, shortcode);

			tb_remove();

		 });

		  /* --- Display the Random Ad - [random_ad] --- */
		var form_random = jQuery('<div id="random-ad-form"><div id="random-ad" class="mce-section"><span class="section-settings"><label for="id">Id: <input type="text" name="id" id="id" value="" /><small>*Required non-duplicate</small></label>\
		<label for="seconds_shelf_life">Shelf Life (Seconds): <input type="text" name="seconds_shelf_life" id="seconds_shelf_life" value="" /><small>Defaults to 86400 seconds (1 day)</small></label></span>\
		<p class="submit"><input type="button" id="random-ad-submit" class="button-primary" value="Insert Random Ad Settings" name="submit" /></p></div></div>');

		var table = form_random.find('div');
		form_random.appendTo('body').hide();

		form_random.find('#random-ad-submit').click(function() {

		var options = {
			'id' : '',
			'seconds_shelf_life' : ''
			};

			var shortcode = '[random_ad';

			for(var index in options){
				var value = jQuery('#random-ad #' + index).val();

				if(value !== options[index])
					shortcode += ' ' + index + '="' + value + '"';

			}

			shortcode += ']';

			tinyMCE.activeEditor.execCommand('mceInsertContent', 0, shortcode);

			tb_remove();

		 });

		  /* --- Display the Stationary Ad - [stationary_ad] --- */
		var form_stationary = jQuery('<script type="text/javascript" src="../wp-content/plugins/SDPackage/includes/admin/javascript/media/sd-sc-stationary-image.js"></script><div id="stationary-ad-form"><div id="stationary-ad" class="mce-section sc-form"><span class="section-settings">\
		<input class="sd-sc-stationary-media button-primary" type="button" value="Select Image">\
		<label for="image_url">Image URL: <input type="text" name="image_url" id="image_url" value="" /><small>* Required</small></label>\
		<label for="image_title">Image Title: <input type="text" name="image_title" id="image_title" value="" /></label>\
		<label for="alt_text">Image Alt: <input type="text" name="alt_text" id="alt_text" value="" /><small>Defaults to Promotion</small></label>\
		<label for="link_to">Link To: <input type="text" name="link_to" id="link_to" value="" /></label>\
		<label for="image_caption">Caption <input type="text" name="image_caption" id="image_caption" value="" /></label>\
		<label for="class">Class: <input type="text" name="class" id="class" value="" /></span>\
		<p class="submit"><input type="button" id="stationary-ad-submit" class="button-primary" value="Insert Stationary Ad Settings" name="submit" /></p></div></div>');

		var table = form_stationary.find('div');
		form_stationary.appendTo('body').hide();

		form_stationary.find('#stationary-ad-submit').click(function() {

		var options = {
			'image_url' : '',
			'image_title' : '',
			'alt_text' : '',
			'link_to' : '',
			'class' : '',
			'image_caption' : '',
			};

			var shortcode = '[stationary_ad';

			for(var index in options){
				var value = jQuery('#stationary-ad #' + index).val();

				if(value !== options[index])
					shortcode += ' ' + index + '="' + value + '"';

			}

			shortcode += ']';

			tinyMCE.activeEditor.execCommand('mceInsertContent', 0, shortcode);

			tb_remove();

		 });

		 /* --- Display the Videos List - [videos_list] --- */
		var form_videos = jQuery('<div id="videos-list-form"><div id="videos-list" class="mce-section"><span class="section-settings"><label for="number_posts">Number of Videos to Display: <input type="text" name="number_posts" id="number_posts" value="" /><small>Defaults to 3</small></label></span>\
		<p class="submit"><input type="button" id="videos-list-submit" class="button-primary" value="Insert Videos List Settings" name="submit" /></p></div></div>');

		var table = form_videos.find('div');
		form_videos.appendTo('body').hide();

		form_videos.find('#videos-list-submit').click(function() {

		var options = {
			'number_posts' : '',
			};

			var shortcode = '[videos_list';

			for(var index in options){
				var value = jQuery('#videos-list #' + index).val();

				if(value !== options[index])
					shortcode += ' ' + index + '="' + value + '"';

			}

			shortcode += ']';

			tinyMCE.activeEditor.execCommand('mceInsertContent', 0, shortcode);

			tb_remove();

		 });

		  /* --- Display the What's New - [whats_new] --- */
		var form_new = jQuery('<div id="whats-new-form"><div id="whats-new" class="mce-section"><span class="section-settings"><label for="number_posts">Number of Posts to Display: <input type="text" name="number_posts" id="number_posts" value="" /><small>Defaults to 5</small></label>\
		<label for="link">Link to: <input type="text" name="link" id="link" value="" /><small>Defaults to siteurl + /category/events/</small></label></span>\
		<p class="submit"><input type="button" id="whats-new-submit" class="button-primary" value="Insert What\'s New Settings" name="submit" /></p></div></div>');

		var table = form_new.find('div');
		form_new.appendTo('body').hide();

		form_new.find('#whats-new-submit').click(function() {

		var options = {
			'number_posts' : '',
			'link' : '',
			};

			var shortcode = '[whats_new';

			for(var index in options){
				var value = jQuery('#whats-new #' + index).val();

				if(value !== options[index])
					shortcode += ' ' + index + '="' + value + '"';

			}

			shortcode += ']';

			tinyMCE.activeEditor.execCommand('mceInsertContent', 0, shortcode);

			tb_remove();

		 });

		  /* --- Display the What's Next - [whats_next] --- */
		var form_next = jQuery('<div id="whats-next-form"><div id="whats-next" class="mce-section"><span class="section-settings"><label for="number_posts">Number of Posts to Display: <input type="text" name="number_posts" id="number_posts" value="" /><small>Defaults to 5</small></label>\
		<label for="calendar">Calendar URL: <input type="text" name="calendar" id="calendar" value="" /><small>* Required</small></label>\
		<label for="link">Link to: <input type="text" name="link" id="link" value="" /><small>Defaults to siteurl + /events/calendar/calendar-list-view/</small></label></span>\
		<p class="submit"><input type="button" id="whats-next-submit" class="button-primary" value="Insert What\'s Next Settings" name="submit" /></p></div></div>');

		var table = form_next.find('div');
		form_next.appendTo('body').hide();

		form_next.find('#whats-next-submit').click(function() {

		var options = {
			'number_posts' : '',
			'calendar' : '',
			'link' : '',
			};

			var shortcode = '[whats_next';

			for(var index in options){
				var value = jQuery('#whats-next #' + index).val();

				if(value !== options[index])
					shortcode += ' ' + index + '="' + value + '"';

			}

			shortcode += ']';

			tinyMCE.activeEditor.execCommand('mceInsertContent', 0, shortcode);

			tb_remove();

		 });

	  /* --- Download Content - [download] --- type url title subtitle */
		var form_downloadContent = jQuery('<script type="text/javascript" src="../wp-content/plugins/SDPackage/includes/admin/javascript/media/sd-download-link.js"></script><div id="downloable-content-form"><div id="downloable-content" class="mce-section"><span class="section-settings"><label for="downloadtype">Download Type <br/><select id="downloadtype" name="downloadtype"><option value="content">General Content - Image, Video, Sound Sample, etc.</option><option value="music-single">Music Single - Artist Sample Music, Free Downloadable Music, etc.</option><option value="documents-files">Documents and Files - Contracts, Invoices, ZIPs, PDFs, DOCs, etc.</option><option value="press-package">Press Package - WYWP Brochures, Virtual Business Cards, Service Pamphlet, etc. </option><option value="dropbox">Dropbox Content - Files hosted on Dropbox.com </option><option value="google-drive">Google Drive - Content hosted on Google Drive </option></select><small>Defaults to General Content ("content")</small></label>\
		<label for="url">Download URL: &nbsp; <input class="sd-open-download button-primary" type="button" value="Select File"> <input type="text" name="url" id="url" value="" /><small>* Required</small></label>\
		<label for="downloadtitle">Title: <input type="text" name="downloadtitle" id="downloadtitle" value="" /></label><label for="subtitle">Subtitle: <input type="text" name="subtitle" id="subtitle" value="" /></label></span>\
		<p class="submit"><input type="button" id="downloable-content-submit" class="button-primary" value="Insert Download Button" name="submit" /></p></div></div>');

		var table = form_downloadContent.find('div');
		form_downloadContent.appendTo('body').hide();

		form_downloadContent.find('#downloable-content-submit').click(function() {

		var options = {
			'downloadtype' : '',
			'url' : '',
			'downloadtitle' : '',
			'subtitle' : '',
			};

			var shortcode = '[download';

			for(var index in options){
				var value = jQuery('#downloable-content #' + index).val();

				if(value !== options[index])
					shortcode += ' ' + index + '="' + value + '"';

			}

			shortcode += ']';

			tinyMCE.activeEditor.execCommand('mceInsertContent', 0, shortcode);

			tb_remove();

		 });
	});
})();