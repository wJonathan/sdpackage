<?php
/* --- sd_package_support_documents - Support Documents Page --- */
define('PLUGIN_PATH', WP_PLUGIN_URL.'/'.str_replace(basename(__FILE__),"",plugin_basename(__FILE__)));
$page = 'support-documents';
$tabs = true;
$tab_content = <<<EOD
    <li><a href="javascript:tabSwitch(1, 2, 'sd-tab-', 'sd-content-');" title="Introduction" class="active" id="sd-tab-1">Introduction</a></li>
    <li><a href="javascript:tabSwitch(2, 2, 'sd-tab-', 'sd-content-');" title="Quick Reference Guide" id="sd-tab-2">Quick Reference Guide</a></li>
EOD;

require_once(dirname(__FILE__).'/includes/sd_package_masthead.php');?>

<div  id="sd-content-1">

    <div class="settings-section half-section odd">
    
        <div class="section-title">
            <h4 class="title">Need Help?</h4>
        </div>
        <!-- .section-title -->
        
        <div class="section-content">
            <p>If you need help with anything, feel free to <a href="mailto:customerservices@saavedradesigns.com" target="_blank" title="Conact Saavedra Design">contact us</a>.</p>
            <p><strong>Phone:</strong> +1(951)732-7044</p>
            <p><strong>Email:</strong> <a href="mailto:customerservices@saavedradesigns.com" target="_blank" title="Email Customer Support">customerservices@<br/>saavedradesigns.com</a></p>
        </div>
        <!-- .section-content -->
        
        <div class="section-actions">
            <form action="mailto:customerservices@saavedradesigns.com" method="get" target="_blank">
            <p class="center"><button class="button-primary">Email Us</button></p>
            </form>
        </div>
        <!-- .section-actions -->
        
    </div>
    <!-- .settings-section -->
    
    <div class="settings-section half-section even" id="cms-guide">
    
        <div class="section-title">
            <h4 class="title">Custom CSM Guide</h4>
        </div>
        <!-- .section-title -->
        
        <div class="section-content">
            <p>Are you having difficulties updating and editing your WordPress powered website? It's okay. </p>
            <p>Simply take a quick look your <em>Custom CMS Guide</em> (see links below) or take a gander at the <em>Quick Reference Guide</em> (see above).</p>
        </div>
        <!-- .section-content -->
        
        <div class="section-actions">
            <form action="http://saavedradesigns.com/?client=100" method="get" target="_blank" id="cms-guide-read">
            <p class="center"><button class="button-primary">Read</button></p>
            </form>
            <form action="http://saavedradesigns.com/?client=100" method="get" target="_blank"  id="cms-guide-download">
            <p class="center"><button class="button-primary">Download</button></p>
            </form>
        </div>
        <!-- .section-actions -->
        
    </div>
    <!-- .settings-section -->
    
    <div class="float-catch"></div>
    
</div>
<!-- #sd-content-1 -->

<div  id="sd-content-2">
    <div class="settings-section">
    
        <div class="section-title">
            <h4 class="title">Support Documents</h4>
        </div>
        <!-- .section-title -->
        
        <div class="section-content">
            <p>The SD Package Settings Panel was developed to help you maintain and edit your custom plugins more efficiently. All the necessary settings for the listed plugins can be in their respected sections.</p>
            <p></p>
        </div>
        <!-- .section-content -->
        
    </div>
    <!-- .settings-section -->
    
</div>
<!-- #sd-content-2 -->
                    
<?php require_once(dirname(__FILE__).'/includes/sd_package_footer.php'); ?>