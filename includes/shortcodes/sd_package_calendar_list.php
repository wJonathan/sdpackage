<?php 
/* --- 
Calendar List Shortcode

Implements a shortcode to use the display Calendar List
--- */


/* --- Register new Shortcode - [calendar_list] --- */

add_shortcode('calendar_list', 'sd_package_calendar_list');

/* --- The callback function that will replace - [calendar_list] --- */

function sd_package_calendar_list($attr){
	
	/* --- Set post count default --- */
	$defaults = array(
	
	'number_posts' => 3,
	'calendar' => get_option('calendar_list_feed_url'),
	'link' => site_url().'/events/calendar/calendar-list-view/',
	'list_title' => ''
	
	);
	
	/* --- Set defaults if omitted --- */
	extract(shortcode_atts($defaults, $attr));
	
	/* --- Get the calendar ready for display --- */
		$output = '<div class="shortcode sd_package_calendar_list">';
		if(isset($list_title) && $list_title != ''){
			$output .= '<h3 class="widget-title">'.$list_title.'</h3><ul class="calendar-list">';	
		}else{
			$output .= '<ul class="calendar-list">';
		}
	
	/* --- Set the current Date --- */
	date_default_timezone_set('America/Los_Angeles');
	
	/* --- Set Today's Date --- */
	$today = date('Y-m-'.date('d', mktime(0, 0, 0, date('m'), date('d')-1, date('Y'))).'\TH:i:sP');
	
	/* --- Create new Dom Object --- */
	$dom_object = new DOMDocument();
	
	/* --- Load XML -- */
	$dom_object->load($calendar."?orderby=starttime&sortorder=ascending&max-results=".$number_posts."&start-min=".date("c"));
			 
	/* --- Get the Entry from the XML --- */
	$item = $dom_object->getElementsByTagName("entry");
	$i = 0;
	
	/* --- Get the Content from the XML --- */		 
	foreach( $item as $value ){
		$titles = $value->getElementsByTagName("title");
		$title  = $titles->item(0)->nodeValue;
			 
		$times = $value->getElementsByTagName("when");
		$startTime = $times->item(0)->getAttributeNode("startTime")->value;
		$endTime = $times->item(0)->getAttributeNode("endTime")->value;
		$eventDate = date( "M d", strtotime( $startTime ) );
		$time = date( "F j, Y - h:i A", strtotime( $startTime ) );
		$endingtime = date( "F j, Y - h:i A", strtotime( $endTime ) );
			 
		$location = $value->getElementsByTagName("where");
		$eventLocation = $location->item(0)->getAttributeNode("valueString")->value;
		$eventURL= $value->getElementsByTagName("link");
		$eventLinkURL=$eventURL->item(0)->getAttributeNode("href")->value;
		
		/* --- Setup output for the Event List --- */
		$output .= '<li class="event-item"><span class="event-date"><h4 class="event-date">'.$eventDate.'</h4></span><span class="event-description"><h4 class="event-title">'.$title.'</h4><p><strong>When:</strong> '.$time.' to '.$endingtime.'</p><p><strong>Where:</strong> '.$eventLocation.'</p><p><a href="'.$eventLinkURL.'" title="Event Details" target="_blank" >More Details...</a></p></span><span class="float-catch"></span></li>';
			 $i++;
			 
	}
			
	$output .= "</ul><p>Showing ".$i." upcoming events.</p></div>";

	/* --- Return the output --- */
	return $output;
}

?>