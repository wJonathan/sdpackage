/**
 * Thanks to mojowill For the Tutorial on Thomas Griffin's Github Script
 *
 * https://github.com/thomasgriffin/New-Media-Image-Uploader
 * http://www.mojowill.com/developer/using-the-new-wordpress-3-5-media-manager-in-your-plugin-or-theme/
 *
*/

jQuery(document).ready(function($) {
	//Prepare the variable to hold our custom madia manager.
	var sd_media_frame;
	var formlabel = 0;
	
	//Bind our click event in order to open up a new media experience
	$(document.body).on('click.sdOpenMediaManager', '.sd-sc-stationary-media', function(e) { 
		//sd-open-media will be our form button
		
		//Prevent the default action from occuring.
		e.preventDefault();
		
		//Get our parent element
		formlabel = jQuery(this).parent();
		//alert(formlabel.value);
		
		
		//If frame already exists, re-open it.
		if(sd_media_frame){
			sd_media_frame.open();
			return;
		}
		sd_media_frame = wp.media.frames.sd_media_frame = wp.media({
		
		//Create our media frame
		className: 'media-frame sd_media_frame',
		frame: 'select', //Allow select only
		multiple: false, //Disallow multiople selections
		library: {
			type: 'image' //Allow only images
			},
		});
		sd_media_frame.on('select', function() {
			//Grab our attachment selection and construct a JSON representation of the model.
			var media_attachment = sd_media_frame.state().get('selection').first().toJSON();
			
			//Send the attachment URL to our custom input field via jQuery.
			formlabel.find('input[name="image_url"] ').val(media_attachment.url);
			//Send the attachment Title to our custom input field via jQuery.
			formlabel.find('input[name="image_title"]').val(media_attachment.title);
			//Send the attachment Alt to our custom input field via jQuery.
			formlabel.find('input[name="alt_text"]').val(media_attachment.alt);
			//Send the attachment URL to our custom Image Src field via jQuery.
			formlabel.find('input[name="image_caption"]').val(media_attachment.caption);
			
		});
		sd_media_frame.open();
	 });
});