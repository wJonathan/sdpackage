[+before_widget+]
	<a href ="[+link+]" target="_blank" title="[+image_title+]"><figure><img src="[+image_url+]" alt="[+alt_text+]" /><figcaption>[+image_caption+]</figcaption></figure></a>
[+after_widget+]