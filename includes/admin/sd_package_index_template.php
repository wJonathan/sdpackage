<?php
/* --- sd_package_index - Main Settings Page --- */
 
define('PLUGIN_PATH', WP_PLUGIN_URL.'/'.str_replace(basename(__FILE__),"",plugin_basename(__FILE__)));

$page = "name-of-page";

$tabs = true;
$tab_content = <<<EOD
<li><a href="" title="Tab Title" class="active ">Tab Title</a></li>
EOD;

require_once(dirname(__FILE__).'/includes/sd_package_masthead.php');?>

<div class="settings-section">
    <div class="section-title">
        <h4 class="title">Section Title</h4>
    </div>
    <!-- .section-title -->
    <div class="section-content">
        <p>Section Content Goes Here</p>
    </div>
    <!-- .section-content -->
    <div class="section-actions">
        <form id="" action="" method="get">
            <input type="text" class="wide-input">
            <input type="reset" name="reset_defaults_1" class="reset" value="Reset">
            <input type="submit" name="sd_some_action_1" id="sd_some_actions" class="button" value="Some Action">
        </form>
    </div>
    <!-- .section-actions -->
</div>
<!-- .settings-section -->
<div class="settings-section half-section odd">
    <div class="section-title">
        <h4>Section Title</h4>
    </div>
    <!-- .section-title -->
    <div class="section-content">
        <p>Section Content Goes Here</p>
    </div>
    <!-- .section-content -->
    <div class="section-actions">
        <form id="" action="" method="get">
            <input type="reset" name="reset_defaults_2" class="reset" value="Reset">
            <input type="submit" name="sd_some_action_2" id="sd_some_actions" class="button" value="Some Action">
        </form>
    </div>
    <!-- .section-actions -->
</div>
<div class="settings-section half-section even">
    <div class="section-title">
        <h4>Section Title</h4>
    </div>
    <!-- .section-title -->
    <div class="section-content">
        <p>Section Content Goes Here</p>
    </div>
    <!-- .section-content -->
    <div class="section-actions">
        <form id="" action="" method="get">
            <input type="reset" name="reset_defaults_2" class="reset" value="Reset">
            <input type="submit" name="sd_some_action_2" id="sd_some_actions" class="button" value="Some Action">
        </form>
    </div>
    <!-- .section-actions -->
</div>
<div class="float-catch"></div>
<!-- .settings-section -->

<?php require_once(dirname(__FILE__).'/includes/sd_package_footer.php'); ?>