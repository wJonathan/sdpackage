<?php
/* sd_package_ad_management - Ad Management */
$page = 'ad-management';
print $msg; 
define('PLUGIN_PATH', WP_PLUGIN_URL.'/'.str_replace(basename(__FILE__),"",plugin_basename(__FILE__)));

/* --- Get the Ads out of the database --- */
$items = get_option('content_rotation_content_block');
// 0:URL 1:Title 2:Alt 3:Link 4:Caption/// Order in Array
?>

<script language="javascript">
/* --- Populate the Page with Form Elements --- */
jQuery(function(){
	/* --- Identify the Placement Div --- */
	var scntDiv = jQuery('.promotions');
	/* --- Get the $items into Javascript using Json --- */
	var currentPromos = <?php echo json_encode($items); ?>;
	/* --- Display all of the Current Fields --- */
	for(var j = 0; j < currentPromos.length; j++){
		jQuery('<p><span class="thumbnail"><img src="'+currentPromos[j][0]+'" alt="'+currentPromos[j][2]+'" class="img_thumb" /></span>\
		<span class="section-settings two-col"><label for="promo_title">Title: <input type="text" id="content" class="field wide-input" name="title[]" value="'+currentPromos[j][1]+'" /></label>\
		<label for="promo_link">Link To: <input type="text" id="content" class="field wide-input" name="content_promo_link[]" value="'+currentPromos[j][3]+'" /></label>\
		<label for="promo_caption">Caption: <textarea name="caption[]" class="wide-input" row="3" col="20">'+currentPromos[j][4]+'</textarea></label>\
		<label for="promo_alt_text">Alt Text: <input type="text" id="content" class="field wide-input" name="alt_text[]" value="'+currentPromos[j][2]+'" /></label>\
		<label for="promo_url">Url: <input type="text" id="content" class="field wide-input" name="image_url[]" value="'+currentPromos[j][0]+'" /></label>\
		<span class="setting-actions two-buttons"><button class="sd-open-media button-primary" type="button"><?php echo _e('Upload Image', 'random_ad'); ?></button><a href="#" id="remove_promotion_button" class="button-secondary">Remove</a></span></span></p>').appendTo(scntDiv);
	}
});

/* --- Functions for Adding and Removing Form Elements --- */
jQuery(function() { 
	/* --- Get the current number of form elements --- */
	var i = jQuery('.promotions p').size() + 1;
	/* --- Set the Placement div for all new elements --- */
	var scntDiv = jQuery('.promotions');
	/* --- Set the Add Promotion and the content that gets added and to where --- */
	jQuery('#add_promotion_button').live('click', function(){
		jQuery('<p><span class="thumbnail"><img src="" alt="promo_thumb" class="img_thumb" /></span>\
			<span class="section-settings two-col"><label for="promo_title">Title: <input type="text" id="content" class="field wide-input" name="title[]" value="" /></label>\
			<label for="promo_link">Link To: <input type="text" id="content" class="field wide-input" name="content_promo_link[]" value="" /></label>\
			<label for="promo_caption">Caption: <textarea name="caption[]" class="wide-input" row="3" col="20"></textarea></label>\
			<label for="promo_alt_text">Alt Text: <input type="text" id="content" class="field wide-input" name="alt_text[]" value="" /></label>\
			<label for="promo_url">URL: <input type="text" id="content" class="field wide-input" name="image_url[]" value="" /></label>\
			<span class="setting-actions two-buttons"><button class="sd-open-media button-primary" type="button"><?php echo _e('Upload Image', 'random_ad'); ?></button><a href="#" id="remove_promotion_button" class="button-secondary">Remove</a></span></span></p>').appendTo(scntDiv);
		i++;
		return false;
	});
	
	/* --- Set the Remove Promotion function that removes the entire <p></p> --- */
	jQuery('#remove_promotion_button').live('click', function() {
		if(i > 2){
			jQuery(this).parents('p').remove();
			i--;
		}
	
	});
	
 });
</script>

<?php 
$tabs = true;

$tab_content = <<<EOD
    <li><a href="javascript:tabSwitch(1, 2, 'sd-tab-', 'sd-content-');" title="Random Advertisement" class="active" id="sd-tab-1">Random Advertisement</a></li>
    <li><a href="javascript:tabSwitch(2, 2, 'sd-tab-', 'sd-content-');" title="Stationary Advertisement" id="sd-tab-2">Stationary Advertisement</a></li>
EOD;

require_once(dirname(__FILE__).'/includes/sd_package_masthead.php');?>
		<div  id="sd-content-1">
        <div class="settings-section half-section odd">
            <div class="section-title">
                <h4 class="title">How Does it Work?</h4>
            </div>
            <!-- .section-title -->
            <div class="section-content">
                <p>A random advertisement can be placed on a page and/or post via the [SD] button in the editor- or through a Widget under Appearance. The editor will allow you to populate a shortcode (displayed below) that was created specifically for What You Want Productions.</p> 
                <p><strong>The Shortcode:</strong></p>
                <p class="info"><code>[random_ad id="" seconds_shelf_life=""]</code></p>
                <p><strong>What is the <code>id</code>? </strong></p>
                <p>The <code>id</code> is a unique identifier that allows the database to store the advertisement properly. </p>
                <p class="attn">The <code>id</code> should <strong>NOT</strong> be duplicated and is required.</p>
                
                <p><strong>What is the <code>seconds_shelf_life</code>? </strong></p>
                <p>The <code>seconds_shelf_life</code> is how long the advertisement displays (in seconds) from page load to the next page load.</p>
                <p class="attn">For a random advertisement every time you load the page, set the <code>seconds_shelf_life</code> to zero (0): <code>seconds_shelf_life="0"</code></p>
            </div>
            <!-- .section-content -->
            
        </div>
        <!-- .settings-section -->
        
        <div class="settings-section half-section even">
            <div class="section-title">
                <h4 class="title">Seconds_Shelf_Life Reference</h4>
            </div>
            <!-- .section-title -->
            <div class="section-content">
                <table class="sec-life">
                	<tr class ="header">
                    	<th>Duration </th>
                        <th class="sec">Seconds</th>
                    </tr>
                    <tr class="odd">
                    	<td>1 Hour</td>
                        <td class="sec">3600</td>
                    </tr>
                    <tr class="even">
                    	<td>12 Hours</td>
                        <td class="sec">4320</td>
                    </tr>
                    <tr class="odd">
                    	<td>1 Day (<em>Default</em> / 24 Hours)</td>
                        <td class="sec">86400</td>
                    </tr>
                    <tr class="even">
                    	<td>1 Week (7 Days)</td>
                        <td class="sec">604800</td>
                    </tr>
                    <tr class="odd">
                    	<td>1 Month (30 Days)</td>
                        <td class="sec">2592000</td>
                    </tr>
                </table> 
            	<p>We have provided you with this chart for your convenience.</p>
            </div>
            <!-- .section-content -->
            
        </div>
        <!-- .settings-section -->
        
        <div class="settings-section half-section even">
            <div class="section-title">
                <h4 class="title">How to Add an Ad to the List?</h4>
            </div>
            <!-- .section-title -->
            <div class="section-content">
                <p><strong>Simply click on the "<em>Add an Advertisement</em>" button below and completely fill out the form provided.</strong></p> 
                <p>All the content requested is used to display the advertisements in your website both on desktop browser formats as well as on mobile phone formats.</p>
                
                <p class="attn"><strong class="break">Mobile Advertisement Example: </strong><strong class="break"><a href="#">Loveliest 2020 [ or Title]</a></strong>Inside Loveliest 2020 Fans [or Caption]</p>
            </div>
            <!-- .section-content -->
            
        </div>
        <!-- .settings-section -->
        
        <div class="float-catch"></div>
        
        <div class="settings-section">
            <div class="section-title">
                <h4 class="title">Random Advertisement List</h4>
            </div>
            <!-- .section-title -->
            <div class="section-content">
            </div>
            <!-- .section-content -->
            <div class="section-actions">
                <input id="add_promotion_button" type="button" value="Add an Advertisement" class="button-secondary" />
                <form id="content_rotation_admin_options_form" action="" method="post">
                    <div class="setting-form promotions">
                    </div>
                    <!-- .promotions -->
                    <?php 
wp_nonce_field('content_rotation_admin_options_update','content_rotation_admin_nonce'); ?>
<input type="submit" name="submit" id="sd_save_changes" class="button-primary" value="Save Settings" /> 
                </form>
            </div>
            <!-- .section-actions -->
        </div>
        <!-- .settings-section -->
        </div>
        <!-- #sd-content-1 -->
        <div class="settings-section" id="sd-content-2">
            <div class="section-title">
                <h4 class="title">How Does it Work?</h4>
            </div>
            <!-- .section-title -->
            <div class="section-content">
                <p>A stationary advertisement can be placed on a page and/or post via the [SD] button in the editor- or through a Widget under Appearance. The editor will allow you to populate a shortcode (displayed below) that was created specifically for What You Want Productions.</p> 
                
                <p><strong>The Shortcode:</strong></p>
                <p class="info"><code>[stationary_ad image_url="" image_title="" alt_text="" link_to="" class=""]</code></p>
                
                <p><strong>What is the <code>image_url</code>?</strong></p>
                <p>The URL to the image of the advertisement.</p>
                
                <p><strong>What is the <code>image_title</code>?</strong></p>
                <p><code>image_title</code> is a descriptive action related to the image/ad (e.g. "Join us for LoveFest 2020").The <code>image_title</code> helps improve SEO optimization. The <code>image_title</code> is also used in the mobile phone formats as the text for the ad link.</p>
                
                <p><strong>What is the <code>image_caption</code>?</strong></p>
                <p><code>image_caption</code> is a descriptive text (longer than <code>image_title</code>) related to the image/ad- preferably 80 characters or less (e.g. "Awesome music from DjChesire! December 2020. Buy tickets now."). The <code>image_caption</code> is only used in the mobile phone formats as additional text for ad link.</p>
                
                <p><strong>What is the <code>alt_text</code>?</strong></p>
                <p>The <code>alt_text</code> is a short description of the image. <code>alt_text</code> helps screen readers read your website as well as improve SEO optimization. The <code>alt_text</code> is also in the mobile phone formats as the text for the link.</p>
                
                <p><strong>What is the <code>link_to</code>?</strong></p>
                <p><code>link_to</code> is basically the URL that you want the visitor to be directed to when they click on the image/ad.</p>
                
                <p><strong>What is the <code>class</code>?</strong></p>
                <p>The <code>class</code> helps to target it using CSS. It should be one word or a hyphenated word.</p>
            </div>
            
        </div>
        <!-- .settings-section -->

<?php require_once(dirname(__FILE__).'/includes/sd_package_footer.php'); ?>