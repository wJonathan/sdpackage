<?php
/* ---
sd_package_what_were_planning extends WP_Widget

This implements a Wordpress widget to display what we are planning
--- */

class sd_package_what_were_planning extends WP_Widget
	{
		public $name = 'SD - What We\'re Planning';
		public $description = 'Displays What We\'re Planning';
		/* ---
		   List all controllable options here along with a default value.
		   The Values can be distinct for each instance of the widget.
		--- */
		public $control_options = array(
		);
		
		//!!! Magic Functions
		// The constructor
		
		function __construct(){
			$widget_options = array(
				'classname' => __CLASS__,
				'description' => $this->description,
			);
			parent::__construct(__CLASS__, $this->name,$widget_options,$this->control_options);
		}
		
		/* ---
		   Display the widget form in the manager, used for editing its settings
		--- */
		public function form($instance){
			print ('Please use the SD Package Dashboard');
		} 
		
		//!!! Static Functions
		static function register_this_widget(){
			register_widget(__CLASS__);
		}
		
		/* ---
		   Display content to the front-end.
		--- */
		function widget($args, $instance){
			$instance['title'] = sd_package_functions::get_content($instance, 'what_were_planning_title');
			$instance['content'] = sd_package_functions::get_content($instance, 'what_were_planning_content');
			$placeholders = array_merge($args, $instance);
			$tpl = file_get_contents(dirname(dirname(dirname(__FILE__))).'/templates/basic_widget.tpl');
			print wpautop( sd_package_functions::parse($tpl, $placeholders), false );
		}
		
	}
	
	/* --- EOF --- */

?>