<?php
/* --- sd_package_index - Main Settings Page --- */

define('PLUGIN_PATH', WP_PLUGIN_URL.'/'.str_replace(basename(__FILE__),"",plugin_basename(__FILE__)));

$tabs = false;

require_once(dirname(__FILE__).'/includes/sd_package_masthead.php');?>

                <div class="settings-section">
                    <div class="section-title">
                        <h4 class="title">Introduction</h4>
                    </div>
                    <!-- .section-title -->
                    <div class="section-content">
                        <p>The SD Package Settings Panel was developed to help you maintain and edit your custom plugins more efficiently. All the necessary settings for the listed plugins can be found and edited in their respected sections.</p>
                        <p></p>
                    </div>
                    <!-- .section-content -->
                </div>
                <!-- .settings-section -->
                <div class="settings-section">
                    <div class="section-title">
                        <h4 class="title">[SD] Button</h4>
                    </div>
                    <!-- .section-title -->
                    <div class="section-content">
                        <p class="half-section even">Numerous shortcodes were created- for your convenience- to be used throughout your website. (<a href="?page=SD_Package_Support_Documents" title="Please refer to the documentation for more information on shortcodes">Please refer to the documentation for more information on shortcodes</a>.) The [SD] button, pictured to the left, lists all the shortcodes we created, so you don't have to remember them all.</p>
                        <aside class="half-section odd"><img src="<?php echo PLUGIN_PATH; ?>/images/sd-btn.jpg" alt="[SD] Button Preview" width="238px" height="155px" /></aside>
                        <p class="float-catch"></p>
                    </div>
                    <!-- .section-content -->
                </div>
                <!-- .settings-section -->
                <div class="settings-section">
                    <div class="section-title">
                        <h4 class="title">Client Pages Default Password</h4>
                    </div>
                    <!-- .section-title -->
                    <div class="section-content">
                        <p class="half-section odd"> By default all client pages are password protected. In case you forget to add a custom password for your clients, the default password is: </p>
                        <aside class="half-section even"><div class="attn"><code>fe6aSwUn</code></div></aside>
                        <p class="float-catch"></p>
                    </div>
                    <!-- .section-content -->
                </div>
                <!-- .settings-section -->
                <div class="settings-section half-section odd">
                    <div class="section-title">
                        <h4 class="title">Need Help?</h4>
                    </div>
                    <!-- .section-title -->

                    <div class="section-content">
                        <p>If you need help with anything, feel free to <a href="mailto:customerservices@saavedradesigns.com" target="_blank" title="Conact Saavedra Design">contact us</a>.</p>
                        <p><strong>Phone:</strong> +1(951)732-7044</p>
                        <p><strong>E-mail:</strong> <a href="mailto:customerservices@saavedradesigns.com" target="_blank" title="Email Customer Support">customerservices@<br/>saavedradesigns.com</a></p>
                    </div>
                    <!-- .section-content -->

                    <div class="section-actions">
                        <form action="mailto:customerservices@saavedradesigns.com" method="get" target="_blank"  >
                        <p class="center">
                          <button class="button-primary">E-mail Us</button></p>
                        </form>
                    </div>
                    <!-- .section-actions -->

                </div>
                <!-- .settings-section -->
                <div class="settings-section half-section even">
                    <div class="section-title">
                        <h4 class="title">Website Maintenance</h4>
                    </div>
                    <!-- .section-title -->
                    <div class="section-content">
                        <p>We can help you maintain your new WordPress website.</p>
                        <p>From monthly, yearly to even pay-as-you-go plans, we have a diverse selection of plans that cater to all business types and sizes. </p>
                    </div>
                    <!-- .section-content -->
                    <div class="section-actions">
                        <form action="http://saavedradesigns.com/?client=100" method="get" target="_blank">
                        <p class="center"><button class="button-primary">Find Out More</button></p>
                        </form>
                    </div>
                    <!-- .section-actions -->

<?php require_once(dirname(__FILE__).'/includes/sd_package_footer.php'); ?>