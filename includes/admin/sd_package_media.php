<?php
/* --- sd_package_media - Media Settings Page --- */
print $msg;
define('PLUGIN_PATH', WP_PLUGIN_URL.'/'.str_replace(basename(__FILE__),"",plugin_basename(__FILE__)));
$page = "media";

/* --- Get the Ads out of the database --- */
$items = get_option('music_list');
// 0:URL 1:Title /// Order in Array
?>
<script language="javascript">
/* --- Populate the Page with Form Elements --- */
jQuery(function(){
	/* --- Identify the Placement Div --- */
	var scntDiv = jQuery('.songs');
	/* --- Get the $items into Javascript using Json --- */
	var currentSongs = <?php echo json_encode($items); ?>;
	/* --- Display all of the Current Fields --- */
	for(var j = 0; j < currentSongs.length; j++){
		jQuery('<p><span class="section-settings"><label for="song_title">Title: <input type="text" id="content" class="field wide-input" name="title[]" value="'+currentSongs[j][1]+'" /></label>\
		<label for="mp3">MP3: <input type="text" id="content" class="field wide-input" name="mp3[]" value="'+currentSongs[j][0]+'" /></label>\
		<label for="ogg">OGG: <input type="text" id="content" class="field wide-input" name="ogg[]" value="'+currentSongs[j][2]+'" /></label>\
		<span class="setting-actions two-buttons"><button class="sd-open-mp3 button-primary" type="button"><?php echo _e('Upload MP3', 'sd_music_mp3'); ?></button>\
		<span class="setting-actions two-buttons"><button class="sd-open-ogg button-primary" type="button"><?php echo _e('Upload OGG', 'sd_music_ogg'); ?></button>\
		<a href="#" id="remove_song_button" class="button-secondary">Remove</a></span></span></p>').appendTo(scntDiv);
	}
});

/* --- Functions for Adding and Removing Form Elements --- */
jQuery(function() {
	/* --- Get the current number of form elements --- */
	var i = jQuery('.songs p').size() + 1;
	/* --- Set the Placement div for all new elements --- */
	var scntDiv = jQuery('.songs');
	/* --- Set the Add Song and the content that gets added and to where --- */
	jQuery('#add_song_button').live('click', function(){
		jQuery('<p><span class="section-settings"><label for="song_title">Title: <input type="text" id="content" class="field wide-input" name="title[]" value="" /></label>\
		<label for="mp3">MP3: <input type="text" id="content" class="field wide-input" name="mp3[]" value="" /></label>\
		<label for="ogg">OGG: <input type="text" id="content" class="field wide-input" name="ogg[]" value="" /></label>\
		<span class="setting-actions two-buttons"><button class="sd-open-mp3 button-primary" type="button"><?php echo _e('Upload MP3', 'sd_music_mp3'); ?></button>\
		<button class="sd-open-ogg button-primary" type="button"><?php echo _e('Upload OGG', 'sd_music_ogg'); ?></button>\
		<a href="#" id="remove_song_button" class="button-secondary">Remove</a></span></span></p>').appendTo(scntDiv);
		i++;
		return false;
	});

	/* --- Set the Remove Song function that removes the entire <p></p> --- */
	jQuery('#remove_song_button').live('click', function() {
		if(i > 2){
			jQuery(this).parents('p').remove();
			i--;
		}

	});

 });
</script>

<?php
$tabs = true;
$tab_content = <<<EOD
	<li><a href="javascript:tabSwitch(1, 4, 'sd-tab-', 'sd-content-');" title="Header Image" class="active" id="sd-tab-1">Header Image</a></li>
	<li><a href="javascript:tabSwitch(2, 4, 'sd-tab-', 'sd-content-');" title="Music" id="sd-tab-2">Music</a></li>
	<li><a href="javascript:tabSwitch(3, 4, 'sd-tab-', 'sd-content-');" title="Photos" id="sd-tab-3">Photos</a></li>
	<li><a href="javascript:tabSwitch(4, 4, 'sd-tab-', 'sd-content-');" title="Videos" id="sd-tab-4">Videos</a></li>
EOD;

require_once(dirname(__FILE__).'/includes/sd_package_masthead.php');?>

<div id="sd-content-1">

    <div class="settings-section">
        <div class="section-title">
            <h4 class="title">Header Image Settings</h4>
        </div>
        <!-- .section-title -->

        <!--<div class="section-content">
        </div>-->
        <!-- .section-content -->

        <div class="section-actions">
            <form id="header_image_admin_options_form" action="" method="post">
                <div class="setting-form"><p><span class="thumbnail"><img src="<?php echo get_option('header_image_url'); ?>" alt="promo_thumb" class="img_thumb" /></span>
                <span class="section-settings two-col">
                    <label for="header_title">Title: <input type="text" id="header_image_title" class="header_image_title wide-input" name="title[]" value="<?php echo get_option('header_image_title'); ?>" /></label>
                    <label for="header_image_linkto">Link to: <input type="text" id="header_image_linkto" class="header_image_linkto wide-input" name="header_image_link" value="<?php echo get_option('header_image_link'); ?>" /></label>
                    <label for="header_image_caption">Caption: <textarea name="caption[]" row="3" cols="20" class="wide-input" ><?php echo get_option('header_image_caption'); ?> </textarea></label>
                    <label for="header_image_alt">Alt: <input type="text" id="header_image_alt" class="header_image_alt wide-input" name="alt_text[]" value="<?php echo get_option('header_image_alt_text'); ?>" /></label>
                    <label for="header_image_url">Image URL: <input type="text" id="header_image_url" class="header_image_url wide-input" name="image_url[]" value="<?php echo get_option('header_image_url'); ?>" /></label>


                <span class="setting-actions"><button class="sd-open-media button-primary" type="button"><?php echo _e('Upload Image', 'random_ad'); ?></button></span></span></p></div><input type="hidden" name="Section" value="header_image">
                    <?php wp_nonce_field('header_image_admin_options_update','header_image_admin_nonce');
                    ?>

                    <input type="submit" name="submit" class="button-primary" value="Save Settings" />

            </form>
        </div>
        <!-- .section-actions -->
    </div>
    <!-- .settings-section -->
</div>
<!-- #sd-content-1 -->

<div id="sd-content-2">

    <div class="settings-section half-section odd">

        <div class="section-title">
            <h4 class="title">How Does it Work?</h4>
        </div>
        <!-- .section-title -->

        <div class="section-content">
            <p>The <em>Song List</em> (see below) is not displayed anywhere. It's simply used to create a randomized playlist.</p>
        </div>
        <!-- .section-content -->

        <!--<div class="section-actions">
        </div>
        <!-- .section-actions -->

    </div>
    <!-- .settings-section -->
    <div class="settings-section half-section even">

        <div class="section-title">
            <h4 class="title">How to Add a Song to the List</h4>
        </div>
        <!-- .section-title -->

        <div class="section-content">
            <p>To add a song, is simple. Simply click on the "<em>Add Song</em>" button below and fill out the form.</p>
        </div>
        <!-- .section-content -->

        <!--<div class="section-actions">
        </div>
        <!-- .section-actions -->

    </div>
    <!-- .settings-section -->
	<div class="float-catch"></div>
    <div class="settings-section">

        <div class="section-title">
            <h4 class="title">Media Converter</h4>
        </div>
        <!-- .section-title -->

        <div class="section-content">
            <p>For the music player to function properly throughout all popular browsers, two files must be present- an MP3 and an OGG. If you don't have a preferred media converter, may we recommend <a href="http://media.io" title="Covert your MP3s to OGG files" target="_blank">http://media.io</a>. </p>
        </div>
        <!-- .section-content -->

        <!--<div class="section-actions">
        </div>
        <!-- .section-actions -->

    </div>
    <!-- .settings-section -->
    <div class="settings-section">

        <div class="section-title">
            <h4 class="title">Song List</h4>
        </div>
        <!-- .section-title -->

        <!--<div class="section-content">
        </div>-->
        <!-- .section-content -->

        <div class="section-actions">
            <input id="add_song_button" type="button" value="Add Song" class="button-secondary" />
            <form id="music_list_admin_options_form" action="" method="post">
                <div class="setting-form songs">
                </div>
                <input type="hidden" name="Section" value="music_list">
                <?php wp_nonce_field('music_list_admin_options_update','music_list_admin_nonce'); ?>
                <input type="submit" name="submit" class="button-primary" value="Save Settings" />
            </form>
        </div>
        <!-- .section-actions -->

    </div>
    <!-- .settings-section -->

</div>
<!-- #sd-content-2 -->

<div class="settings-section" id="sd-content-3">
    <div class="section-title">
        <h4 class="title">Most Recent Photos</h4>
    </div>
    <!-- .section-title -->
    <div class="section-content">
        <p>This simple gallery displays the most recent photos, in "albums".</p>
        <p>The <em>Most Recent Photos</em> section may be added to any post/page by using the [SD] button in the editor. The editor will allow you to populate a shortcode that was created specifically for What You Want Productions. </p>

        <p><strong>The Shortcode:</strong></p>
        <p class="info"><code>[photos_list number_posts=""]</code></p>

        <p><strong>What is <code>number_posts</code>?</strong></p>
        <p>The <code>number_posts</code> is how many albums you would like to display. Default is three (3).</p>
    </div>
    <!-- .section-content -->
</div>
<!-- .settings-section -->
<div class="settings-section" id="sd-content-4">
    <div class="section-title">
        <h4 class="title">Most Recent Videos</h4>
    </div>
    <!-- .section-title -->
    <div class="section-content">
        <p>The <em>Most Recent Videos</em> section mat be added to any post/page by using the [SD] button in the editor. The editor will allow you to populate a shortcode that was created specifically for What You Want Productions.</p>

        <p><strong>The Shortcode:</strong></p>
        <p class="info"><code>[videos_list number_posts=""]</code></p>

        <p><strong>What is <code>number_posts</code>?</strong></p>
        <p>The <code>number_posts</code> is how many albums you would like to display. Default is three (3).</p>
    </div>
<!-- .section-actions -->
</div>
<!-- .settings-section -->

<?php require_once(dirname(__FILE__).'/includes/sd_package_footer.php');

if(!empty($_POST) && $_POST['Section'] == 'music_list'){
	?>
	<script language="javascript">
		document.getElementById('sd-tab-2').className = "active";
		document.getElementById('sd-tab-1').className = "";
		document.getElementById('sd-content-2').style.display = "block";
		document.getElementById('sd-content-1').style.display = "none";
	</script>
	<?php
}
?>