<?php
/* ---
sd_package_stationary_ad extends WP_Widget

This implements a Wordpress widget designed to display a Stationary Ad or Image
--- */

class sd_package_stationary_ad extends WP_Widget
	{
		public $name = 'SD - Stationary Advertisement';
		public $description = 'Stationary Advertisement';
		/* ---
		   List all controllable options here along with a default value.
		   The Values can be distinct for each instance of the widget. 
		--- */
		public $control_options = array(
			'image_url' => '',
			'link' => '',
			'image_title' => '',
			'alt_text' => '',
			'image_caption' =>'',
		);
		
		//!!! Magic Functions
		// The constructor
		
		function __construct(){
			$widget_options = array(
				'classname' => __CLASS__,
				'description' => $this->description,
			);
			parent::__construct(__CLASS__, $this->name,$widget_options,$this->control_options);
		}
		
		/* ---
		   Display the widget form in the manager, used for editing its settings
		--- */
		public function form($instance){
			wp_enqueue_media(); //To use the Media Library v3.5
			$sd_plugindir = WP_PLUGIN_URL.'/'.str_replace(basename(__FILE__),"../",plugin_basename(__FILE__));
			wp_enqueue_script('sd_wi_script', $sd_plugindir.'admin/javascript/media/sd-wi-image.js'); //Custom use for the Media Library v3.5
			wp_enqueue_script('jQuery');

			print '<input class="sd-wi-media button-primary" type="button" value="Select Image"><br />';
			$placeholders = array();
			
			foreach($this->control_options as $key => $val){
				$placeholders[ $key.'.id'] = $this->get_field_id($key);
				$placeholders[ $key.'.name'] = $this->get_field_name($key);
				
				if(isset($instance[$key])){
					$placeholders[ $key.'.value'] = esc_attr($instance[$key]);
				}else{
					$placeholders[ $key.'.value'] = $this->control_options[$key];
				}
			}
			$tpl = file_get_contents(dirname(dirname(dirname(__FILE__))).'/templates/stationary_ad_widget_controls.tpl');
			print sd_package_functions::parse($tpl, $placeholders);
		} 
		
		//!!! Static Functions
		static function register_this_widget(){
			register_widget(__CLASS__);
		}
		
		/* ---
		   Display content to the front-end.
		--- */
		function widget($args, $instance){
			$placeholders = array_merge($args, $instance);
			$tpl = file_get_contents(dirname(dirname(dirname(__FILE__))).'/templates/stationary_ad_widget.tpl');
			print sd_package_functions::parse($tpl, $placeholders);
		}
	}
	
	/* --- EOF --- */

?>